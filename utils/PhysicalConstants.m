classdef PhysicalConstants

    properties
        
        T0 = -273.15             % zero Kelvin in Celcius
        R = 8.3144626            % Gas constant (value from https://en.wikipedia.org/wiki/Gas_constant)
        molH2O0                  % Number of moles in 1kg of pure water.
        molarmass
        
    end
    
    methods
        
        function obj = PhysicalConstants()
            
            molarmass = containers.Map();
            molarmass('H2O')  = 18.01528*milli*kilogram;
            molarmass('CO2')  = 44.0095*milli*kilogram;
            molarmass('NaCl') = 58.44277*milli*kilogram;
            molH2O0 = 1./molarmass('H2O');

            obj.molarmass = molarmass;
            obj.molH2O0 = molH2O0;
            
        end
        
    end
    
end
