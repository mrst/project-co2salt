In this code we propose an implementation of the reduced model for a CO2-Water-Salt system developped by Pruess et al (see reference below). 

The implementation is based on [MRST](https://www.sintef.no/projectweb/mrst/) which will be downloaded and installed as
a submodule (just follow the installation instructions below).

Overview of the results and implementation are given in slide presentations in the `notes` directory.

# Main scripts overview #

- `runCO2Water` : runs equilibrium composition for a water-CO2 system with a pressure range from 0 to 600 barsa
- `runDuanSun` : runs equilibrium composition for a water-CO2-**salt** system with a pressure range from 0 to 600 barsa with different salt molar fraction

    ![Comparison Result](/img/comparison.png ) 
- `runTransport` : runs a 1D transport problem with injection on the left of the domain

# Installation #

Install the repoco

```matlab
git clone --recurse-submodules  https://bitbucket.org/mrst/project-co2salt.git
```

Then start MATLAB and in the directory where you cloned the repository, run:

```matlab
startupCo2Salt
```

You can check that that your installation is setup correctly by running one of the example scripts :

```matlab
runDuanSun
```

# Reference papers #

- *CO2-H2O mixtures in the geological sequestration of CO2. I. Assessment and calculation of mutual solubilities from 12 to 100 C and up to 600 bar* Spycher, Nicolas and Pruess, Karsten and Ennis-King, Jonathan, Geochimica et cosmochimica acta, 2003
- *CO2-H2O Mixtures in the Geological Sequestration of CO2. II. Partitioning in Chloride Brines at 12--100 C and up to 600 bar* Spycher, Nicolas and Pruess, Karsten, Geochimica et Cosmochimica Acta, 2005
- *ECO2M: a TOUGH2 fluid property module for mixtures of water, NaCl, and CO2, including super-and sub-critical conditions, and phase change between liquid and gaseous CO2*., Pruess, Karsten, 2011
