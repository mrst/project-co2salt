classdef DuanSunModel < CompositeModel
% Salt model.
%
% We use the following references
%
% @article{ref1,
%   title={CO2-H2O Mixtures in the Geological Sequestration of CO2. II. Partitioning in Chloride Brines at 12--100 C and up to 600 bar},
%   author={Spycher, Nicolas and Pruess, Karsten},
%   journal={Geochimica et Cosmochimica Acta},
%   volume={69},
%   number={13},
%   pages={3309--3320},
%   year={2005},
%   publisher={Elsevier}
% }
%

    properties
        
        T % Temperature
        
        vstoich % Stoichiometric number of ions (2 for NaCl, 3 for CaCl2, ...)
        
        physconst
        compmolarmass

        nP = 2;
        nC = 3;
        
    end
   
    methods
        
        function model = DuanSunModel(T)
            
            model = model@CompositeModel('duansun');
            
            model.T = T;
            model.physconst = PhysicalConstants();
            
            model.vstoich = 2; % (for example NaCl)
            
            compmolarmass{1} = model.physconst.molarmass('H2O');
            compmolarmass{2} = model.physconst.molarmass('CO2');
            compmolarmass{3} = model.physconst.molarmass('NaCl');
            model.compmolarmass = compmolarmass;
            
            submodel = CO2WaterModel(T);
            submodel = submodel.setAlias({'pressure', VarName({'..'}, 'pressure')});
            names = submodel.names;
            [~, lia] = ismember('pressure', names);
            names(lia) = [];
            submodel.names = names;
            model.SubModels{1} = submodel;

            names = {'pressure'           , ... % Pressure
                     'compmass'           , ... % Component Mass (= component density)
                     'phaseMolarFractions', ... % Phase Molar fraction
                     'phaseMassFractions' , ... % Phase Mass fraction
                     'rho'                , ... % Phase densities
                     'rhoBrine'           , ... % Brine density (without CO2)
                     'activity'           , ... % Activity coefficient for the salt
                     'molsalt'             ... % number of mol in 1 kg of pure water (molality)
                    };
            
            model.names = names;
            
            nP = model.nP;
            nC = model.nC;            
            vardims = model.vardims;
            vardims('compmass') = nC;
            vardims('rho') = nP;
            vardims('phaseMolarFractions') = 5;
            vardims('phaseMassFractions') = 5;
            
            aliases = {{'rhoA' , VarName({'.'}, 'rho', 2, 1)}, ... % Density aqueous phase
                       {'rhoG' , VarName({'.'}, 'rho', 2, 2)}, ... % Density gas/Co2-rich phase
                       {'mH2O' , VarName({'.'}, 'compmass', 3, 1)}, ...
                       {'mCO2' , VarName({'.'}, 'compmass', 3, 2)}, ...
                       {'mA'   , VarName({'.'}, 'phaseMassFractions',  5, (1 : 3))}, ...  %  aqueous phase
                       {'mG'   , VarName({'.'}, 'phaseMassFractions',  5, (4 : 5))}, ...  %  gas/Co2-rich phase
                       {'xA'   , VarName({'.'}, 'phaseMolarFractions', 5,  (1 : 3))}, ... %  aqueous phase
                       {'xG'   , VarName({'.'}, 'phaseMolarFractions', 5,  (4 : 5))}, ... %  gas/Co2-rich phase
                       {'yH2O' , VarName({'.'}, 'xG', 2, 1)}, ...
                       {'xCO2' , VarName({'.'}, 'xA', 3, 2)}, ...
                       {'xsalt', VarName({'.'}, 'xA', 3, 3)}, ...
                      };
            model.aliases = aliases;
            
                        
            fn = @(model, state) model.computeActivityCoefficient(state);
            inputnames = {'molsalt', 'pressure'};
            model = model.addPropFunction('activity', fn, inputnames, {'.'}); %

            fn = @(model, state) model.computeMolarFractions(state);
            inputnames = {'molsalt', 'pressure', 'activity', ...
                          VarName({'co2water'}, 'KH2O'), ...
                          VarName({'co2water'}, 'phiH2O'), ...
                          VarName({'co2water'}, 'xH2O'), ...
                         };
            model = model.addPropFunction('phaseMolarFractions', fn, inputnames, {'.'}); %
            
            fn = @(model, state) model.computeDensities(state);
            inputnames = {'xA', 'xG', 'mA', 'mG', ....
                          VarName({'co2water'}, 'KH2O'), ...
                          'rhoBrine', ...
                          VarName({'co2water'}, 'rhoDissCO2')};
            model = model.addPropFunction('rho', fn, inputnames, {'.'}); %
            
            fn = @(model, state) model.computeMassFractions(state);
            inputnames = {'pressure', 'xA', 'xG'};
            model = model.addPropFunction('phaseMassFractions', fn, inputnames, {'.'}); %

            fn = @(model, state) model.computeBrineWaterDensity(state);
            inputnames = {'pressure', 'molsalt'};
            model = model.addPropFunction('rhoBrine', fn, inputnames, {'.'}); %
            
            model = model.initiateCompositeModel();
            
        end
        
        function state = computeActivityCoefficient(model, state)
        % from ref 1, equation (A-4)
            
            T = model.T;
            
            [molsalt, state] = model.getUpdatedProp(state, 'molsalt');
            [P, state] = model.getUpdatedProp(state, 'pressure');
            
            P = P/barsa; % we convert to bar
            lambda = -0.411370585 + 6.07632013e-4*T + 97.5347708/T - 0.0237622469*P/T + 0.0170656236*P/(630 - T) + ...
                     1.41335834e-5*T*log(P);
            xi = 3.36389723e-4 -1.98298980e-5*T + 2.12220830e-3*P/T - 5.24873303e-3*P/(630 - T);
            
            lngamma = 2*lambda.*molsalt + xi.*molsalt.^2; % for NaCl
            gamma = exp(lngamma);
            
            state = model.setProp(state, 'activity', gamma);
            
        end
        
        function state = computeMolarFractions(model, state)
           
            vstoich = model.vstoich;
            molH2O0 = model.physconst.molH2O0;
            
            [molsalt, state] = model.getUpdatedProp(state, 'molsalt');
            [P, state] = model.getUpdatedProp(state, 'pressure');
            [gamma, state] = model.getUpdatedProp(state, 'activity');
            
            vmolsalt = vstoich*molsalt;
            
            co2water = model.getAssocModel({'co2water'});
            [KH2O, state]   = co2water.getUpdatedProp(state, 'KH2O');
            [phiH2O, state] = co2water.getUpdatedProp(state, 'phiH2O');
            [xCO2, state]   = co2water.getUpdatedProp(state, 'xCO2');
            
            molCO2 = xCO2./(1 - xCO2).*(molH2O0 + vmolsalt);
            molCO2 = molCO2./gamma;
            
            xCO2 = molCO2./(molH2O0 + vmolsalt + molCO2);
            xsalt = vmolsalt./(molH2O0 + vmolsalt + molCO2);
            
            A = KH2O./(phiH2O.*P);
            yH2O = A.*(1 - xCO2 - xsalt);

            xA{1} = (1 - xCO2 - xsalt);
            xA{2} = xCO2;
            xA{3} = xsalt;
            
            xG{1} = yH2O; 
            xG{2} = 1 - yH2O;                
            
            state = model.setProp(state, 'xA', xA);
            state = model.setProp(state, 'xG', xG);
            
        end

        function state = computeMassFractions(model, state)
            
            [p, state] = model.getUpdatedProp(state, 'pressure');
            [xA, state] = model.getUpdatedProp(state, 'xA');
            [xG, state] = model.getUpdatedProp(state, 'xG');

            nC = model.nC;
            cm = model.compmolarmass;
            vstoich = model.vstoich;
            n = numelValue(p);
            mAtot = zeros(n, 1);
            mGtot = zeros(n, 1);
            
            for ind = 1 : (nC - 1)
                mAtot = mAtot + xA{ind}*cm{ind};
                mGtot = mGtot + xG{ind}*cm{ind};
            end
            mAtot = mAtot + xA{nC}./vstoich*cm{nC}; % add salt contribution (only water phase)
            
            for ind = 1 : (nC - 1)
                mA{ind} = (xA{ind}.*cm{ind})./mAtot;
                mG{ind} = (xG{ind}.*cm{ind})./mGtot;
            end
            mA{nC} = (xA{nC}.*cm{nC}./vstoich)./mAtot; % add salt contribution (only water phase)

            state = model.setProp(state, 'mA', mA);
            state = model.setProp(state, 'mG', mG);
            
        end

        function state = computeDensities(model, state)
            
            [vG, state] = model.getUpdatedProp(state, {'co2water', 'vG'});
            [xA, state] = model.getUpdatedProp(state, 'xA');
            [xG, state] = model.getUpdatedProp(state, 'xG');
            [mA, state] = model.getUpdatedProp(state, 'mA');
            [mG, state] = model.getUpdatedProp(state, 'mG');
            
            [rhob, state] = model.getUpdatedProp(state, 'rhoBrine');
            [rhoc, state] = model.getUpdatedProp(state, {'co2water', 'rhoDissCO2'});
            
            % Aqueous phase density, see ref2 section 3.1
            invrhoA = (mA{1} + mA{3})./rhob + mA{2}./rhoc;
            rhoA = 1./invrhoA;
            
            % CO2 rich phase density
            m = model.physconst.molarmass;
            mG = xG{1}.*m('H2O') + xG{2}.*m('CO2');
            rhoG = mG./vG;
            
            state = model.setProp(state, 'rhoA', rhoA);
            state = model.setProp(state, 'rhoG', rhoG);
    
        end
        
        
        function state = computeBrineWaterDensity(model, state)

            T = model.T;
            [p, state] = model.getUpdatedProp(state, 'pressure');
            [molsalt, state] = model.getUpdatedProp(state, 'molsalt');
            
            % handle case where there is no water (molsalt = NaN)
            molsalt(isnan(value(molsalt))) = 0;
            
            % unit conversion : pressure difference from 1 atmosphere in barsa
            P = (p - 1*atm)/barsa;
            % unit conversion : temperature in celsius
            T = T + model.physconst.T0;
            % unit conversion : salinity in g/kg
            mm = model.physconst.molarmass('NaCl');
            molsalt = mm*molsalt*milli*kilogram;
            
            [~, rho, ~] = Seawaterdensity_International_highpressure_Calc(T, molsalt, P);
            
            state = model.setProp(state, 'rhoBrine', rho);
            
        end
    end
    
end

