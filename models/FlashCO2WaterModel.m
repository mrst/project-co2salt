classdef FlashCO2WaterModel < ComponentModel
% CO2-water model.
%
% We use the following references
%
% @article{ref1,
%   title={CO2-H2O mixtures in the geological sequestration of CO2. I. Assessment and calculation of mutual solubilities from 12 to 100 C and up to 600 bar},
%   author={Spycher, Nicolas and Pruess, Karsten and Ennis-King, Jonathan},
%   journal={Geochimica et cosmochimica acta},
%   volume={67},
%   number={16},
%   pages={3015--3031},
%   year={2003},
%   publisher={Elsevier}
% }

    
    properties
        
        nC = 2;
        nP = 2;
        T

        compmolarmass  % component molar mass
        co2watermodel
        
        physconst
        
        TC = 31 + 273.15         % Critical temperature of pure CO2
        
    end
   
    methods
        
        function model = FlashCO2WaterModel(T)
            
            model = model@ComponentModel('flash');
            
            model.T = T;
            model.physconst = PhysicalConstants();
        
            model.co2watermodel = CO2WaterModel(T);
        
            compmolarmass{1} = model.physconst.molarmass('H2O');
            compmolarmass{2} = model.physconst.molarmass('CO2');
            model.compmolarmass = compmolarmass;
            
            
            names = {'pressure', ...           % Pressure
                     'phaseMassFractions', ... % Phase Mass fraction
                     'compmass', ...           % Component Mass (= component density)
                     'rho', ...                % phase densities
                     's', ...                  % saturations
                     'phaseindex', ...         % phase index
                    };
            model.names = names;
           
            nC = model.nC;
            aliases = {{'mH2O' , VarName({'.'}, 'compmass', 1)}, ...
                       {'mCO2' , VarName({'.'}, 'compmass', 2)}, ...
                       {'mA'   , VarName({'.'}, 'phaseMassFractions', 1 : nC)}, ... 
                       {'mG'   , VarName({'.'}, 'phaseMassFractions', nC + (1 : nC))}, ... 
                       {'sA'   , VarName({'.'}, 's', 1)}, ... 
                       {'sG'   , VarName({'.'}, 's', 2)}, ... 
                       {'rhoA' , VarName({'.'}, 'rho', 1)}, ...  
                       {'rhoG' , VarName({'.'}, 'rho', 2)}, ...
                      };
            model.aliases = aliases;
            
            propfunctions = {};
            
            fn = @(model, state) model.solveFlash(state);
            propfunctions{end + 1} = PropFunction('rho', fn, {'.'});
            propfunctions{end + 1} = PropFunction('phaseMassFractions', fn, {'.'});
            propfunctions{end + 1} = PropFunction('s', fn, {'.'});
            
            model.propfunctions = propfunctions;
            
        end
        
        function state = initiateState(model, state)
            
            state = initiateState@ComponentModel(model, state);
            nC = model.nC;
            nP = model.nP;
            
            fieldnames = {'phaseMassFractions', 'compmass', 'rho', 's'};
            ncomps = [nC*nP, nC, nP, nP];
            
            varnames = model.assignCurrentNameSpace(fieldnames);
            
            for ifn = 1 : numel(varnames)
                fieldname = varnames{ifn}.getfieldname;
                ncomp = ncomps(ifn);
                if isempty(state.(fieldname))
                    state.(fieldname) = cell(1, ncomp);
                end
            end
            
        end
        
        function state = solveFlash(model, state)
            
            co2watermodel = model.co2watermodel;
            
            p  = model.getProp(state, 'pressure');
            mCO2 = model.getProp(state, 'mCO2');
            mH2O = model.getProp(state, 'mH2O');

            state2ph.pressure = p;
            state2ph = co2watermodel.initiateState(state2ph);
            
            [xA, state2ph]  = co2watermodel.getUpdatedProp(state2ph, 'xA');
            [xG, state2ph]  = co2watermodel.getUpdatedProp(state2ph, 'xG');
            [rhoA, state2ph] = co2watermodel.getUpdatedProp(state2ph, 'rhoA');
            [rhoG, state2ph] = co2watermodel.getUpdatedProp(state2ph, 'rhoG');
            
            % Convert mol phase fraction in mass phase fraction
            cm = model.compmolarmass;
            nC = model.nC;
            n = size(p, 1);
            
            mAtot = zeros(n, 1);
            mGtot = zeros(n, 1);
            for ind = 1 : nC
                mAtot = mAtot + xA{ind}*cm{ind};
                mGtot = mGtot + xG{ind}*cm{ind};
            end
            
            for ind = 1 : nC
                meqA{ind} = (xA{ind}.*cm{ind})./mAtot;
                meqG{ind} = (xG{ind}.*cm{ind})./mGtot;
            end
            
            n = size(p, 1);
            sA = zeros(n, 1);
            sG = zeros(n, 1);
            [sA, sG] = initVariablesADI(sA, sG);
            
            for ind = 1 : 2
                mA{ind} = meqA{ind};
                mG{ind} = meqG{ind};
            end
            
            eqs{1} = mH2O - rhoA.*sA.*mA{1} - rhoG.*sG.*mG{1};
            eqs{2} = mCO2 - rhoA.*sA.*mA{2} - rhoG.*sG.*mG{2};
            
            eqs = combineEquations(eqs{:});
            
            res = -eqs.jac{1}\eqs.val;
            
            pind = 3*ones(n, 1); 
            
            sA = res(n, 1);
            sG = res(n + (1 : n), 1);
            v = sA + sG;
            
            sA = sA./v;
            sG = sG./v;
            
            % pure aqueous phase
            pA = (sG < 0);
            totmA = mH2O(pA) + mCO2(pA);
            mA{1}(pA) = mH2O(pA)./totmA;
            mA{2}(pA) = mCO2(pA)./totmA;
            mG{1}(pA) = NaN;
            mG{2}(pA) = NaN;
            sA(pA)    = 1;
            sG(pA)    = 0;
            rhoA(pA)  = (mH2O(pA) + mCO2(pA));
            rhoG(pA)  = totmA;
            pind(pA)  = 1;
            
            % pure gas phase
            pG = (sA < 0);
            totmG = mH2O(pG) + mCO2(pG);
            mA{1}(pG) = NaN;
            mA{2}(pG) = NaN;
            mG{1}(pG) = mH2O(pG)./totmG;
            mG{2}(pG) = mCO2(pG)./totmG;
            sA(pG)    = 0;
            sG(pG)    = 1;
            rhoA(pG)  = NaN;
            rhoG(pG)  = totmG;
            pind(pG)  = 2;
            
            state = model.setProp(state, 'mA', mA);
            state = model.setProp(state, 'mG', mG);
            state = model.setProp(state, 'sA', sA);
            state = model.setProp(state, 'sG', sG);            
            state = model.setProp(state, 'rhoA', rhoA);
            state = model.setProp(state, 'rhoG', rhoG);
            state = model.setProp(state, 'phaseindex', pind);
            
        end
        

        
    end
    
end


