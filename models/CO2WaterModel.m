classdef CO2WaterModel < ComponentModel
% CO2-water model.
%
% We use the following references
%
% @article{ref1,
%   title={CO2-H2O mixtures in the geological sequestration of CO2. I. Assessment and calculation of mutual solubilities from 12 to 100 C and up to 600 bar},
%   author={Spycher, Nicolas and Pruess, Karsten and Ennis-King, Jonathan},
%   journal={Geochimica et cosmochimica acta},
%   volume={67},
%   number={16},
%   pages={3015--3031},
%   year={2003},
%   publisher={Elsevier}
% }
%
% @techreport{ref2,
%   doi = {10.2172/1016574},
%   url = {https://doi.org/10.2172%2F1016574},
%   year = 2011,
%   month = {apr},
%   publisher = {Office of Scientific and Technical Information  ({OSTI})},
%   author = {K. Pruess},
%   title = {{ECO}2M: A {TOUGH}2 Fluid Property Module for Mixtures of Water, {NaCl}, and {CO}2, Including Super- and Sub-Critical Conditions, and Phase Change Between Liquid and Gaseous {CO}2}
% }
%

    properties
        T          % Temperature
        P0         % Reference pressure
        K0         % Reference equilibrium constants 
        averVols   % Average partial molar volumes
        RKparams   % Redlich-Kwong parameters
        
        TC = 31 + 273.15 % Critical temperature of pure CO2
        vGC = 94*(centi*meter)^3 % Critical volume of pure CO2

        physconst  % Physical constants
        compmolarmass
        
        nC = 2; % two components
        nP = 2; % two phases
        
    end
   
    methods
        
        function model = CO2WaterModel(T)
            
            model = model@ComponentModel('co2water');
            
            model.T = T;
            model.physconst = PhysicalConstants();

            compmolarmass{1} = model.physconst.molarmass('H2O');
            compmolarmass{2} = model.physconst.molarmass('CO2');
            model.compmolarmass = compmolarmass;
            
            model = model.setParameters();
            
            names = {'pressure'           , ... % Pressure
                     'phaseMolarFractions', ... % Phase Molar fraction
                     'phaseMassFractions' , ... % Phase Mass fraction
                     'phi'                , ... % Fugacity coefficient
                     'Z'                  , ... % Compressibility factor
                     'K'                  , ... % Equilbibrium constants
                     'phasemolarvol'      , ... % Phase molar volume (obtained from cubic, only vG is used)
                     'rho'                , ... % density 
                     'rhoPureWater'       , ... % density pure water (without co2)
                     'rhoDissCO2'         , ... % Partial density of dissolved CO2
                     'critind'            , ... % phase index (1 = subcritical, 2 = supcritical)
                     'nroot'              , ... % number of roots in cubic state
                    };
            model.names = names;
            
            nP = model.nP;
            nC = model.nC;
            vardims = model.vardims;
            vardims('phaseMolarFractions') = nC*nP;
            vardims('phaseMassFractions') = nC*nP;
            vardims('phi') = nC;
            vardims('Z') = nP;
            vardims('phasemolarvol') = nP;
            vardims('rho') = nP;
            vardims('K') = nC;
            model.vardims = vardims;
            
            aliases = {{'phiH2O', VarName({'.'}, 'phi', nC, 1)}, ...
                       {'phiCO2', VarName({'.'}, 'phi', nC, 2)}, ...
                       {'vA', VarName({'.'}, 'phasemolarvol', nP, 1)}, ... % Molar volume for aqeous gas
                       {'vG', VarName({'.'}, 'phasemolarvol', nP, 2)}, ... % Molar volume for gas/CO2-rich phase
                       {'ZA', VarName({'.'}, 'Z', nP, 1)}, ... % Molar volume for aqueous gas
                       {'ZG', VarName({'.'}, 'Z', nP, 2)}, ... % Molar volume for gas/CO2-rich phase
                       {'rhoA', VarName({'.'}, 'rho', nP, 1)}, ... % Density aqueous phase
                       {'rhoG', VarName({'.'}, 'rho', nP, 2)}, ... % Density gas/Co2-rich phase
                       {'mA', VarName({'.'}, 'phaseMassFractions' , 2*nC, 1 : nC)}       , ... 
                       {'mG', VarName({'.'}, 'phaseMassFractions' , 2*nC, nC + (1 : nC))}, ...
                       {'xA', VarName({'.'}, 'phaseMolarFractions', 2*nC, 1 : nC)}       , ... 
                       {'xG', VarName({'.'}, 'phaseMolarFractions', 2*nC, nC + (1 : nC))}, ...
                       {'KH2O', VarName({'.'}, 'K'  , nC, 1)}, ...
                       {'KCO2', VarName({'.'}, 'K'  , nC, 2)}, ...
                       {'xCO2', VarName({'.'}, 'xA' , nC, 2)}, ...
                       {'yH2O', VarName({'.'}, 'xG' , nC, 1)}, ...
                       {'rhoA', VarName({'.'}, 'rho', nP, 1)}, ...  
                       {'rhoG', VarName({'.'}, 'rho', nP, 2)}, ...
                      };
            model.aliases = aliases;
            
            fn = @(model, state) model.computePhi(state);
            inputnames = {'vG', 'pressure'};
            model = model.addPropFunction('phi', fn, inputnames, {'.'});

            fn = @(model, state) model.computeEquilibriumConstants(state);
            inputnames = {'vG', 'pressure'};
            model = model.addPropFunction('K', fn, inputnames, {'.'});
            model = model.addPropFunction('critind', fn, inputnames, {'.'});
            
            fn = @(model, state) model.computeMolarVolumes(state);
            inputnames = {'pressure'};
            model = model.addPropFunction('phasemolarvol', fn, inputnames, {'.'});
            model = model.addPropFunction('nroot', fn, inputnames, {'.'});
            
            fn = @(model, state) model.computeMolarFractions(state);
            inputnames = {'pressure', 'KH2O', 'KCO2', 'phiH2O', 'phiCO2'};
            model = model.addPropFunction('phaseMolarFractions', fn, inputnames, {'.'});
            
            fn = @(model, state) model.computeMassFractions(state);
            inputnames = {'pressure', 'xA', 'xG'};
            model = model.addPropFunction('phaseMassFractions', fn, inputnames, {'.'});
            
            fn = @(model, state) model.computeZ(state);            
            inputnames = {'v', 'pressure'};
            model = model.addPropFunction('Z', fn, inputnames, {'.'});
            
            fn = @(model, state) model.computeDensities(state);
            inputnames = {'vG', 'xA', 'xG', 'mA', 'mG', 'rhoPureWater', 'rhoDissCO2'};
            model = model.addPropFunction('rho', fn, inputnames, {'.'});

            fn = @(model, state) model.computePureWaterDensity(state);
            inputnames = {'pressure'};
            model = model.addPropFunction('rhoPureWater', fn, inputnames, {'.'});

            fn = @(model, state) model.computeDissCo2Dens(state);
            inputnames = {};
            model = model.addPropFunction('rhoDissCO2', fn, inputnames, {'.'});

        end
        
        function model = setParameters(model)
        % We set up the parameters
            
            % Setup Redlich-KKwong parameters. From Reference 1, table 1
            T = model.T;
            
            a =containers.Map();
            a('CO2') = 7.54e7-4.13e4*T; % bar cm^{6} K^{0.5} mol^{–2}
            a('H2O-CO2') = 7.89e7;      % bar cm^{6} K^{0.5} mol^{–2}
            
            b =containers.Map();
            b('CO2') = 27.8;  % cm^{3} mol{-1}
            b('H2O') = 18.18; % cm^{3} mol{-1}
            
            % Conversion to SI unit
            u = barsa*(centi*meter)^6;
            a('CO2')     = u*a('CO2');
            a('H2O-CO2') = u*a('H2O-CO2');
            u = (centi*meter)^3;
            b('CO2')     = u*b('CO2');
            b('H2O')     = u*b('H2O');
    
            model.RKparams = struct('a', a, ...
                                    'b', b);
            
            % Setup equilibrium constants. From Reference 1, table 2
            a = containers.Map();
            a('H2O') = -2.209;
            a('CO2(g)') = 1.189;
            a('CO2(l)') = 1.169;

            b = containers.Map();
            b('H2O')    = 3.097e-2;
            b('CO2(g)') = 1.304e-2;
            b('CO2(l)') = 1.368e-2;

            c = containers.Map();
            c('H2O')    = -1.098e-4;
            c('CO2(g)') = -5.446e-5;
            c('CO2(l)') = -5.380e-5;

            d = containers.Map();
            d('H2O')    = 2.048e-7;
            d('CO2(g)') = 0.0;
            d('CO2(l)') = 0.0;
          
            keys = {'H2O', 'CO2(g)', 'CO2(l)'};
            K0 = containers.Map();
            Tc = T + model.physconst.T0;
            for ind = 1 : numel(keys)
                key = keys{ind};
                K0(key) = 10^(a(key) + b(key)*Tc + c(key)*Tc^2 + d(key)*Tc^3);
                % unit conversion
                K0(key) = K0(key)*barsa;
            end
            
            
            model.K0 = K0;
            
            % From Reference 1, table 2
            averVols = containers.Map();
            u = (centi*meter)^3; % unit conversion
            averVols('H2O')    = 18.1*u;
            averVols('CO2(g)') = 32.6*u;
            averVols('CO2(l)') = 32.6*u;

            model.averVols = averVols;
            
            % From Reference 1, after equation (3)
            model.P0 = 1*barsa;
            

            
        end
        
        function state = computeMolarVolumes(model, state)
            
            R = model.physconst.R;
            T = model.T;
            rk = model.RKparams;

            [P, state] = model.getUpdatedProp(state, 'pressure');
            n = numelValue(P);
            
            amix = rk.a('CO2');
            bmix = rk.b('CO2');
            
            c1 = -(R*T)./P;
            c2 = -(R*T*bmix./P - amix./(P*T^0.5) + bmix^2);
            c3 = -(amix*bmix./(P*T^0.5));
            
            convertAD = @(x, sample) (model.AutoDiffBackend.convertToAD(x, sample));
            V = cubicPositive(c1, c2, c3, convertAD);

            mtol = 0;
            for ind = 1 : 3
                V{ind}(abs(imag(value(V{ind}))) > mtol) = nan;
                V{ind}(V{ind} <= 0) = nan;
                if isa(V{ind}, 'ADI')
                    V{ind}.val = real(V{ind}.val);
                    for ij  = 1 : numel(V{ind}.jac)
                        V{ind}.jac{ij} = real(V{ind}.jac{ij});
                    end
                else
                    V{ind} = real(V{ind});
                end
                
            end

            vA = V{1};
            vG = V{1};
            
            for ind = 1 : 3
                takeA = (V{ind} < vA);
                vA(takeA) = V{ind}(takeA);
                takeG = (V{ind} > vG);
                vG(takeG) = V{ind}(takeG);
            end
            
            nroot = ones(n, 1);
            nroot(vA < vG) = 2;
            
            state = model.setProp(state, 'vA', vA);
            state = model.setProp(state, 'vG', vG);
            state = model.setProp(state, 'nroot', nroot);
            
        end
        
        function state = computePhi(model, state)
            
            R = model.physconst.R;
            T = model.T;
            rk = model.RKparams;
            
            [v, state] = model.getUpdatedProp(state, 'vG');
            [p, state] = model.getUpdatedProp(state, 'pressure');
            
            amix = rk.a('CO2');
            bmix = rk.b('CO2');
            
            phiH2O = log(v./(v - bmix)) + (rk.b('H2O')./(v - bmix)) - 2*rk.a('H2O-CO2')./(R*T^1.5*bmix)*log((v + bmix)./v) ...
                     + (amix*rk.b('H2O'))./(R*T^1.5*bmix^2)*(log((v + bmix)./v) - (bmix./(v + bmix))) - log(p.*v/ ...
                                                              (R*T));
            phiH2O = exp(phiH2O);
            
            phiCO2 = log(v./(v - bmix)) + (rk.b('CO2')./(v - bmix)) - 2*rk.a('CO2')./(R*T^1.5*bmix)*log((v + bmix)./v) ...
                     + (amix*rk.b('CO2'))./(R*T^1.5*bmix^2)*(log((v + bmix)./v) - (bmix./(v + bmix))) - log(p.*v/ ...
                                                              (R*T));
            phiCO2 = exp(phiCO2);
            
            state = model.setProp(state, 'phiH2O', phiH2O);
            state = model.setProp(state, 'phiCO2', phiCO2);
            
        end

        function state = computeEquilibriumConstants(model, state)

            T0 = model.physconst.T0;
            R  = model.physconst.R;

            T  = model.T;
            TC = model.TC;
            K0 = model.K0;
            P0 = model.P0;
            averVols = model.averVols;

            [P, state] = model.getUpdatedProp(state, 'pressure');
            [vG, state] = model.getUpdatedProp(state, 'vG');
            
            n = numelValue(P);
            
            if T  < TC
                issuper = (vG < model.vGC);
            else
                issuper = false(n, 1);
            end
            
            critind = ~issuper.*ones(n, 1) + 2*issuper.*ones(n, 1);
            
            KCO20 = nan(n, 1);
            KCO20(critind == 1) = K0('CO2(g)');
            KCO20(critind == 2) = K0('CO2(l)');
            
            KH2O = K0('H2O')*exp((P - P0).*averVols('H2O')./(R*T));
            KCO2 = KCO20.*exp((P - P0).*averVols('CO2(g)')./(R*T));
            
            state = model.setProp(state, 'KH2O', KH2O);
            state = model.setProp(state, 'KCO2', KCO2);
            state = model.setProp(state, 'critind', critind);

        end
        
        function state = computeMolarFractions(model, state)
           
            [P, state] = model.getUpdatedProp(state, 'pressure');
            [KH2O, state] = model.getUpdatedProp(state, 'KH2O');
            [KCO2, state] = model.getUpdatedProp(state, 'KCO2');
            [phiH2O, state] = model.getUpdatedProp(state, 'phiH2O');
            [phiCO2, state] = model.getUpdatedProp(state, 'phiCO2');
            
            A = KH2O./(phiH2O.*P);
            molH2O0 = model.physconst.molH2O0;
            B = (phiCO2.*P)./(molH2O0.*KCO2);
            
            yH2O = (1 - B)./(1./A - B);
            xCO2 = B.*(1 - yH2O);
            
            xA{1} = 1 - xCO2;
            xA{2} = xCO2;
            xG{1} = yH2O;
            xG{2} = 1 - yH2O;
            
            state = model.setProp(state, 'xA', xA);
            state = model.setProp(state, 'xG', xG);
            
        end

        function state = computeMassFractions(model, state)
            
            [p, state] = model.getUpdatedProp(state, 'pressure');
            [xA, state] = model.getUpdatedProp(state, 'xA');
            [xG, state] = model.getUpdatedProp(state, 'xG');

            nC = model.nC;
            cm = model.compmolarmass;
            n = numelValue(p);
            mAtot = zeros(n, 1);
            mGtot = zeros(n, 1);
            
            for ind = 1 : nC
                mAtot = mAtot + xA{ind}*cm{ind};
                mGtot = mGtot + xG{ind}*cm{ind};
            end
            
            for ind = 1 : nC
                mA{ind} = (xA{ind}.*cm{ind})./mAtot;
                mG{ind} = (xG{ind}.*cm{ind})./mGtot;
            end

            state = model.setProp(state, 'mA', mA);
            state = model.setProp(state, 'mG', mG);
            
        end
        
        function state = computeZ(model, state)
            
            T = model.T;
            R = model.physconst.R;
            
            [v, state] = model.getUpdatedProp(state, 'v');
            [p, state] = model.getUpdatedProp(state, 'pressure');
            
            for ind = 1 : 2
                Z{ind} = (p.*v{ind})/(R*T);
            end
            
            state = model.setProp(state, 'Z', Z);
            
        end
        
        function state = computeDensities(model, state)
            
            [vG, state] = model.getUpdatedProp(state, 'vG');
            [xA, state] = model.getUpdatedProp(state, 'xA');
            [xG, state] = model.getUpdatedProp(state, 'xG');
            [mA, state] = model.getUpdatedProp(state, 'mA');
            [mG, state] = model.getUpdatedProp(state, 'mG');
            
            [rhopw, state] = model.getUpdatedProp(state, 'rhoPureWater');
            [rhoc, state] = model.getUpdatedProp(state, 'rhoDissCO2');
            
            % Aqueous phase density, see ref2 section 3.1
            invrhoA = mA{1}./rhopw + mA{2}./rhoc;
            rhoA = 1./invrhoA;
            
            % CO2 rich phase density
            m = model.physconst.molarmass;
            mG = xG{1}.*m('H2O') + xG{2}.*m('CO2');
            rhoG = mG./vG;
            
            state = model.setProp(state, 'rhoA', rhoA);
            state = model.setProp(state, 'rhoG', rhoG);
            
        end
        
        
        function state = computePureWaterDensity(model, state)
            
            T = model.T;
            [p, state] = model.getUpdatedProp(state, 'pressure');
            
            % unit conversion : pressure difference from 1 atmosphere in barsa
            P = (p - 1*atm)/barsa;
            % unit conversion : temperature in celsius
            T = T + model.physconst.T0;
            
            [~, rho, ~] = Seawaterdensity_International_highpressure_Calc(T, 0, P);
            
            state = model.setProp(state, 'rhoPureWater', rho);
        end

        function state = computeDissCo2Dens(model, state)
        % from ref2, section 3.1
            
            T = model.T;
            a = 37.51;
            b = -9.585e-2;
            c = 8.740e-4;
            d = -5.044e-7;
            
            % unit conversion : temperature in celsius
            T = T + model.physconst.T0;
            V = a + b*T + c*T^2 + d*T^3;
            
            % note that we kg/mol (SI) while ref2 uses g/mol
            mco2 = model.physconst.molarmass('CO2');
            
            rho = (mco2*1e3/V)*1e3;
            
            state = model.setProp(state, 'rhoDissCO2', rho);
            
        end
        
        
    end
    
end

