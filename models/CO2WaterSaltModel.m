classdef CO2WaterSaltModel < CompositeModel
% CO2-water model.
%
% We use the following references
%
% @article{ref1,
%   title={CO2-H2O mixtures in the geological sequestration of CO2. I. Assessment and calculation of mutual solubilities from 12 to 100 C and up to 600 bar},
%   author={Spycher, Nicolas and Pruess, Karsten and Ennis-King, Jonathan},
%   journal={Geochimica et cosmochimica acta},
%   volume={67},
%   number={16},
%   pages={3015--3031},
%   year={2003},
%   publisher={Elsevier}
% }
% @article{ref2,
%  doi = {10.1016/0016-7037(87)90185-2},
%  url = {https://doi.org/10.1016%2F0016-7037%2887%2990185-2},
%  year = 1987,
%  month = {jul},
%  publisher = {Elsevier {BV}},
%  volume = {51},
%  number = {7},
%  pages = {1965--1975},
%  author = {I-Ming Chou},
%  title = {Phase relations in the system {NaCl}-{KCl}-H2O. {III}: Solubilities of halite in vapor-saturated liquids above 445{\textdegree}C and redetermination of phase equilibrium properties in the system {NaCl}-H2O to 1000{\textdegree}C and 1500 bars},
%  journal = {Geochimica et Cosmochimica Acta}
% }

    
    properties
        
        nC  = 3; % number of components (H2O, CO2, salt)
        nP  = 3; % number of phases (A, G, S)
        nFP = 2; % number of fluid phases (A, G)

        H2Ocut = eps;
        CO2cut = eps;
        
        T
        
        solubitlityConstant
        rhosalt
        
        TC = 31 + 273.15 % Critical temperature of pure CO2

        physconst
        compmolarmass
        
    end
   
    methods
        
        function model = CO2WaterSaltModel(T)
            
            model = model@CompositeModel('flash');
            
            model.physconst = PhysicalConstants();
            
            submodel = DuanSunModel(T);
            submodel.modelname = 'twophases';
            submodel = submodel.setAlias({'pressure', VarName({'..'}, 'pressure')});
            names = submodel.names;
            [~, lia] = ismember('pressure', names);
            names(lia) = [];
            submodel.names = names;
            
            model.SubModels{1} = submodel;

            compmolarmass{1} = model.physconst.molarmass('H2O');
            compmolarmass{2} = model.physconst.molarmass('CO2');
            compmolarmass{3} = model.physconst.molarmass('NaCl');
            model.compmolarmass = compmolarmass;
            
            model.T = T;
            model.solubitlityConstant = model.computeSaltSolubility();
            model.rhosalt = 1000;
            
            names = {'pressure'          , ... % Pressure
                     'phaseMassFractions', ... % Phase Mass fraction
                     'phaseCompMass'     , ... % Phase Mass 
                     'compmass'          , ... % Component Mass (= component density)
                     'rho'               , ... % phase densities
                     'phaseVolumes'      , ... % phase volumes
                     's'                 , ... % saturations
                     'phaseindex'        , ... % phase index
                     'phaseflags'        , ... %  A (aqueous) G (co2-rich) S (solid)
                     'saltratio'         , ... % salt ratio (see updateMoleSalt function)
                    };
            model.names = names;

            nP  = model.nP;
            nFP = model.nFP;
            nC  = model.nC;
            vardims = model.vardims;
            
            vardims('phaseMassFractions') = 5;
            
            vardims('phaseCompMass') = 6;
            % phaseCompMass{1} : H2O in A
            % phaseCompMass{2} : CO2 in A
            % phaseCompMass{3} : salt in A
            % phaseCompMass{4} : H2O in G
            % phaseCompMass{5} : CO2 in G
            % phaseCompMass{6} : salt in S
            
            vardims('compmass') = nC;
            vardims('rho') = nFP;
            vardims('s') = nP;
            vardims('phaseVolumes') = nP;
            vardims('phaseFlags') = nP;
            
            aliases = {{'mH2O' , VarName({'.'}, 'compmass', nC, 1)}, ...
                       {'mCO2' , VarName({'.'}, 'compmass', nC, 2)}, ...
                       {'msalt', VarName({'.'}, 'compmass', nC, 3)}, ...
                       {'mA'   , VarName({'.'}, 'phaseMassFractions', 5, (1 : 3))}, ... 
                       {'mG'   , VarName({'.'}, 'phaseMassFractions', 5, (4 : 5))}, ... 
                       {'sA'   , VarName({'.'}, 's', nP, 1)}, ... 
                       {'sG'   , VarName({'.'}, 's', nP, 2)}, ... 
                       {'sS'   , VarName({'.'}, 's', nP, 3)}, ...
                       {'volA' , VarName({'.'}, 'phaseVolumes', nP, 1)}, ... 
                       {'volG' , VarName({'.'}, 'phaseVolumes', nP, 2)}, ...
                       {'volS' , VarName({'.'}, 'phaseVolumes', nP, 3)}, ...
                       {'flagA', VarName({'.'}, 'phaseflags', nP, 1)}, ... 
                       {'flagG', VarName({'.'}, 'phaseflags', nP, 2)}, ...
                       {'flagS', VarName({'.'}, 'phaseflags', nP, 3)}, ...
                       {'rhoA' , VarName({'.'}, 'rho', nFP, 1)}, ...  
                       {'rhoG' , VarName({'.'}, 'rho', nFP, 2)}, ...
                      };
            model.aliases = aliases;

            fn = @(model, state) model.updateMoleSalt(state);
            inputnames = {VarName({'..'}, 'mH2O'), ...
                          VarName({'..'}, 'msalt')};
            model = model.addPropFunction({'twophases', 'molsalt'}, fn, inputnames, {'..'});
            
            
            fn = @(model, state) model.computeFlash(state);
            inputnames = {'pressure', 'mH2O', 'mCO2', 'msalt', 'flags', ...
                          VarName({'twophases'}, 'mA'), ...
                          VarName({'twophases'}, 'mG'), ...
                          VarName({'twophases'}, 'rhoA'), ...
                          VarName({'twophases'}, 'rhoG'), ...
                          VarName({'twophases', 'co2water'}, 'vG'), ...
                          VarName({'twophases'}, 'rhoBrine'), ...
                          VarName({'twophases', 'co2water'}, 'rhoDissCO2'), ...
                         };
            model = model.addPropFunction('phaseMassFractions', fn, inputnames, {'.'});
            model = model.addPropFunction('phaseVolumes', fn, inputnames, {'.'});
            model = model.addPropFunction('s', fn, inputnames, {'.'});
            model = model.addPropFunction('rho', fn, inputnames, {'.'});
            model = model.addPropFunction('phaseindex', fn, inputnames, {'.'});
            
            fn = @(model, state) model.updateMoleSalt(state);
            inputnames = {'mH2O', 'msalt'};            
            model = model.addPropFunction('flagS', fn, inputnames, {'.'});
            model = model.addPropFunction('saltratio', fn, inputnames, {'.'});
            

            fn = @(model, state) model.updatePhaseCompMass(state);
            inputnames = {'s', 'rho', 'phaseMassFractions'};            
            model = model.addPropFunction('phaseCompMass', fn, inputnames, {'.'});            
            
            model = model.initiateCompositeModel();
            
        end

        function state = updateMoleSalt(model, state)
        % compute molsalt : number of mole of salt in 1 kg water
            [mH2O, state] = model.getUpdatedProp(state, 'mH2O');
            [msalt, state] = model.getUpdatedProp(state, 'msalt');
            
            mNaCl = model.physconst.molarmass('NaCl');
            sc = model.solubitlityConstant;

            rmsalt = msalt./mH2O;
            molsalt = rmsalt./mNaCl;
            
            % Check for precipitation and set molsalt to saturated value in case of precipitation
            flagS = (rmsalt > sc);
            molsalt(flagS) = sc/mNaCl;
            rmsalt(flagS) = sc;
            
            state = model.setProp(state, 'saltratio', rmsalt);
            state = model.setProp(state, {'twophases', 'molsalt'}, molsalt);
            state = model.setProp(state, 'flagS', flagS);
            
        end
        

        function [volA, volG, flagH2O, flagCO2, state] = computeVolAG(model, state)
        %
        % returns the volumes of phase A and G that satisfies to  mass conservation (sum up mass contribution in each phase)
        %
        % The resulting volumes can be negative, indicating that the phase will in fact disappear.
        %   
        % The cases where there is no H2O or CO2 are also detected and returned (threshold values are used)
            
            p     = model.getProp(state, 'pressure');
            mH2O  = model.getProp(state, 'mH2O');            
            msalt = model.getProp(state, 'msalt');
            mCO2  = model.getProp(state, 'mCO2');
            
            [meqA, state] = model.getUpdatedProp(state, {'twophases', 'mA'});
            [meqG, state] = model.getUpdatedProp(state, {'twophases', 'mG'});
            [rhoA, state] = model.getUpdatedProp(state, {'twophases', 'rhoA'});
            [rhoG, state] = model.getUpdatedProp(state, {'twophases', 'rhoG'});
            
            flagS = model.getUpdatedProp(state, 'flagS');
            
            [adsample, doAD] = getSampleAD(p, mCO2, mH2O, msalt);
            n = numelValue(p);
            
            volA  = double2ADI(nan(n, 1), adsample);
            volG  = double2ADI(nan(n, 1), adsample);
            
            v = @(x) value(x);
            
            flagH2O = (v(mH2O) > model.H2Ocut);
            flagCO2 = (v(mCO2) > model.CO2cut);
            
            cind = (flagH2O == 1) & (flagCO2 == 1);
            if any(cind)
                
                n = nnz(cind);
                [volA2, volG2] = initVariablesADI(zeros(n, 1), zeros(n, 1));
                
                p     = model.getSliceProp(state, 'pressure', cind);
                mH2O  = model.getSliceProp(state, 'mH2O', cind);            
                msalt = model.getSliceProp(state, 'msalt', cind);
                mCO2  = model.getSliceProp(state, 'mCO2', cind);
                meqA  = model.getSliceProp(state, {'twophases', 'mA'}, cind);
                meqG  = model.getSliceProp(state, {'twophases', 'mG'}, cind);
                rhoA  = model.getSliceProp(state, {'twophases', 'rhoA'}, cind);
                rhoG  = model.getSliceProp(state, {'twophases', 'rhoG'}, cind);
                flagS  = model.getSliceProp(state, 'flagS', cind);
                
            
                totmeqA = v(meqA{1}) + v(meqA{3});
                totMA   = v(mH2O) + v(msalt);
                S.type = '()';
                S.subs = {flagS == 1};
                totmeqA(flagS == 1) = subsref(v(meqA{1}), S);
                totMA(flagS == 1) = subsref(v(mH2O), S);
                
                eqs{1} = totMA   - v(rhoA).*totmeqA.*volA2    - v(rhoG).*v(meqG{1}).*volG2;
                eqs{2} = v(mCO2) - v(rhoA).*v(meqA{2}).*volA2 - v(rhoG).*v(meqG{2}).*volG2;
                
                eqs = combineEquations(eqs{:});
                
                res = -eqs.jac{1}\eqs.val;
                
                volA2 = res(1 : n);
                volG2 = res(n + (1 : n));
                
                if doAD
                    
                    adsample = getSampleAD(p, mCO2, mH2O, meqA, meqG, rhoA, rhoG);
                    numvars = adsample.getNumVars();
                    
                    [volA2, volG2] = initVariablesADI(volA2, volG2);
                    
                    v = @(x) value(x);
                    
                    clear eqs
                        
                    totmeqA = v(meqA{1}) + v(meqA{3});
                    totMA   = v(mH2O) + v(msalt);
                    totmeqA(flagS == 1) = subsref(v(meqA{1}), S);
                    totMA(flagS == 1) = subsref(v(mH2O), S);
                    
                    eqs{1} = totMA   - v(rhoA).*totmeqA.*volA2    - v(rhoG).*v(meqG{1}).*volG2;
                    eqs{2} = v(mCO2) - v(rhoA).*v(meqA{2}).*volA2 - v(rhoG).*v(meqG{2}).*volG2;
                    
                    F1 = combineEquations(eqs{:});
                    
                    clear eqs
                    totmeqA = meqA{1} + meqA{3};
                    totMA   = mH2O + msalt;
                    totmeqA(flagS == 1) = subsref(meqA{1}, S);
                    totMA(flagS == 1) = subsref(mH2O, S);
                    
                    eqs{1} = totMA - rhoA.*totmeqA.*v(volA2) - rhoG.*meqG{1}.*v(volG2);
                    eqs{2} = mCO2 - rhoA.*meqA{2}.*v(volA2) - rhoG.*meqG{2}.*v(volG2);                
                    
                    F2 = combineEquations(eqs{:});
                    
                    jac1 = F1.jac{1};
                    jac2 = horzcat(F2.jac{:});
                    jac = -jac1\jac2;
                    jacvolA2 = jac(1 : n, :);
                    jacvolG2 = jac(n + (1 : n), :);
                    
                    jacvolA2 = mat2cell(jacvolA2, size(jacvolA2, 1), numvars);
                    jacvolG2 = mat2cell(jacvolG2, size(jacvolG2, 1), numvars);
                    
                    volA2 = v(volA2);
                    volG2 = v(volG2);
                    
                    volA2 = ADI(volA2, jacvolA2);
                    volG2 = ADI(volG2, jacvolG2);
                    
                end
                
                volA(cind) = volA2;
                volG(cind) = volG2;
                
            end
            
        end
        
        
        function state = computeFlash(model, state)
            
            rhosalt = model.rhosalt;
            
            [p, state] = model.getUpdatedProp(state, 'pressure');
            [mH2O, state] = model.getUpdatedProp(state, 'mH2O');
            [mCO2, state] = model.getUpdatedProp(state, 'mCO2');
            [msalt, state] = model.getUpdatedProp(state, 'msalt');
            
            [meqA, state] = model.getUpdatedProp(state, {'twophases', 'mA'});
            [meqG, state] = model.getUpdatedProp(state, {'twophases', 'mG'});
            
            for ind = 1 : 3
                mA{ind} = meqA{ind};
                if ind < 3
                    mG{ind} = meqG{ind};
                end
            end

            % initialize rhoA and rhoG
            [rhoeqA, state] = model.getUpdatedProp(state, {'twophases', 'rhoA'});
            [rhoeqG, state] = model.getUpdatedProp(state, {'twophases', 'rhoG'});
            
            rhoA = rhoeqA;
            rhoG = rhoeqG;

            % Initialise phase flags
            [flagS, state]  = model.getUpdatedProp(state, 'flagS');
            [adsample, doAD] = getSampleAD(p, mCO2, mH2O, msalt);
            n = numelValue(p);
            flagA = NaN(n, 1);
            flagG = NaN(n, 1);
            
            % Initialise phase volumes
            volS = double2ADI(NaN(n, 1), adsample);
            [volA, volG, flagH2O, flagCO2, state] = model.computeVolAG(state);
            
            % Update phase flags depending on sign ov volA and volG as returned by method computeVolAG
            
            flagG(volG <= 0) = 0;
            flagA(volA <= 0) = 0;
            flagA((volA > 0) & (volG > 0)) = 1;
            flagG((volA > 0) & (volG > 0)) = 1;

            % Case where H2O is not present

            cind = (flagH2O == 0);
            if any(cind)
                % update volA
                volA(cind) = 0; % volume of A set to zero
                % update volG
                [vG, state] = model.getUpdatedProp(state, {'twophases', 'co2water', 'vG'});
                m = model.physconst.molarmass;
                volG(cind) = vG(cind).*mCO2(cind)./m('CO2');
                % update flagA
                flagA(cind) = 0;
                % update volS
                sind = cind & (flagS == 1);
                volS(sind) = msalt(sind)./rhosalt; 
            end
            
            
            % Case where CO2 is not present

            cind = (flagCO2 == 0);
            if any(cind)
                % update volG
                volG(cind) = 0; % volume of A set to zero
                % update flagG
                flagG(cind) = 0;
                % update volA
                [rhob, state] = model.getUpdatedProp(state, {'twophases', 'rhoBrine'});
                volA(cind) = (mH2O(cind) + msalt(cind))./rhob(cind);
                
                % update volS
                sind = cind & (flagS == 1);
                % msaltA : mass of salt dissolved in water
                msaltA = rhoA(sind).*mA{3}(sind).*volA(sind);
                volS(sind) = (msalt(sind) - msaltA)./rhosalt;
            end
            
            % Case phase S is not present
            
            cind = (flagS == 0);
            if any(cind)
                volS(cind) = 0;
            end
            
            % Case phase G is not present

            cind = (flagG == 0);
            if any(cind)
                
                totmA = mH2O(cind) + mCO2(cind) + msalt(cind);
                
                mA{1}(cind) = mH2O(cind)./totmA;
                mA{2}(cind) = mCO2(cind)./totmA;
                mA{3}(cind) = msalt(cind)./totmA;
                
                [rhob, state] = model.getUpdatedProp(state, {'twophases', 'rhoBrine'});
                [rhoc, state] = model.getUpdatedProp(state, {'twophases', 'co2water', 'rhoDissCO2'});
                invrhoA = (mA{1}(cind) + mA{3}(cind))./rhob(cind) + mA{2}(cind)./rhoc;
                 
                rhoA(cind) = 1./invrhoA;
                rhoG(cind) = NaN;
                
                mG{1}(cind) = NaN;
                mG{2}(cind) = NaN;
                
                % update volA
                volA(cind) = totmA.*invrhoA;
                
                % update volG
                volG(cind) = 0;
                
                % update volS
                sind = cind & (flagS == 1);
                % msaltA : mass of salt dissolved in water
                msaltA = rhoA(sind).*mA{3}(sind).*volA(sind);
                volS(sind) = (msalt(sind) - msaltA)./rhosalt;
                
            end
            
            % Case phase A is not present

            cind = (flagA == 0);
            if any(cind)
                
                flagA(cind) = 0;
                volA(cind) = 0;
                
                totmG = mH2O(cind) + mCO2(cind);
                
                mA{1}(cind) = NaN;
                mA{2}(cind) = NaN;
                mA{3}(cind) = NaN;
                mG{1}(cind) = mH2O(cind)./totmG;
                mG{2}(cind) = mCO2(cind)./totmG;
                
                rhoA(cind) = NaN;
                rhoG(cind) = totmG./volG(cind);
                
                sind = cind & (flagS == 1);
                volS(sind) = msalt(sind)./rhosalt;

            end

            % Case both phases A and G are present

            cind = (flagA == 1) & (flagG == 1);
            
            if any(cind)
                
                mA{1}(cind) = (mH2O(cind) - rhoG(cind).*volG(cind).*mG{1}(cind))./(rhoA(cind).*volA(cind));
                mA{3}(cind) = 1 - mA{1}(cind) - mA{2}(cind);
                
                sind = cind & (flagS == 1);
                % msaltA : mass of salt dissolved in water
                msaltA = rhoA(sind).*mA{3}(sind).*volA(sind);
                volS(sind) = (msalt(sind) - msaltA)./rhosalt;
                
            end
            
            totV = volA + volG + volS;
            sA = volA./totV;
            sG = volG./totV;
            sS = volS./totV;
            
            state = model.setProp(state, 'mA', mA);
            state = model.setProp(state, 'mG', mG);
            state = model.setProp(state, 'sA', sA);
            state = model.setProp(state, 'sG', sG);            
            state = model.setProp(state, 'sS', sS);
            state = model.setProp(state, 'volA', volA);
            state = model.setProp(state, 'volG', volG);
            state = model.setProp(state, 'volS', volS);
            state = model.setProp(state, 'rhoA', rhoA);
            state = model.setProp(state, 'rhoG', rhoG);
            
        end
        

        function solubilityConstant = computeSaltSolubility(model)
            
        % Solubility equation from ref2 (Potter equation)
            
            T = model.T;
            % We convert the temperature in celsius
            T = T + model.physconst.T0;
            
            solubilityConstant = (26.218 + 0.0072*T + 0.000106*T^2)/100;
            
        end
        
        function state = updatePhaseCompMass(model, state);
            
            [s, state] = model.getUpdatedProp(state, 's');
            [rho, state] = model.getUpdatedProp(state, 'rho');
            [pmf, state] = model.getUpdatedProp(state, 'phaseMassFractions');

            zeroAD = model.AutoDiffBackend.convertToAD(zeros(numelValue(s{1}), 1), s{1});
            for ind = 1 : 5
                pcm{ind} = zeroAD;
            end

            v = @(x) value(x);            

            ind = v(s{1} > 0);
            if any(ind)
                massA = rho{1}(ind).*s{1}(ind);
                % H2O in A
                pcm{1}(ind) = massA.*pmf{1}(ind);
                % CO2 in A
                pcm{2}(ind) = massA.*pmf{2}(ind);
                % salt in A
                pcm{3}(ind) = massA.*pmf{3}(ind);
            end
            
            ind = v(s{2} > 0);
            if any(ind)
                massG = rho{2}(ind).*s{2}(ind);
                % H2O in G
                pcm{4}(ind) = massG.*pmf{4}(ind);
                % CO2 in G
                pcm{5}(ind) = massG.*pmf{5}(ind);
            end
            
            % salt in S
            pcm{6} = model.rhosalt*s{3};
            
            state = model.setProp(state, 'phaseCompMass', pcm);
            
        end
        
    end
    
end