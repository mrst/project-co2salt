clear all
close all

mrstModule add compositional ad-core 

T0 = 273.15;
T = 12; % in degrees
T = T + T0;

model = CO2WaterModel(T);
adminmodel = AdminModel();
model = model.setupAdminModel(adminmodel);

doplotgraph = true;
if doplotgraph
    gstyle = {'interpreter', 'none', 'LineWidth', 3, 'ArrowSize', 20, 'NodeFontSize', 14};
    [g, edgelabels] = setupGraph(model);
    figure
    h = plot(g, gstyle{:});
    title('Computational Graph')
end


state.pressure = [1; 600*(0.01 : 0.005 : 1)']*barsa;
state = model.initiateState(state);


% [xL, state] = model.getUpdatedProp(state, 'xL');

% fugacity

% phi = model.getUpdatedProp(state, 'phiH2O');

[yH2O, state]     = model.getUpdatedProp(state, 'yH2O');
[xCO2, state]     = model.getUpdatedProp(state, 'xCO2');
[P, state]        = model.getUpdatedProp(state, 'pressure');
[critind, state]  = model.getUpdatedProp(state, 'critind');
% [ZG, state]     = model.getUpdatedProp(state, 'ZG');
% [phiCO2, state] = model.getUpdatedProp(state, 'phiCO2');
% [phiH2O, state] = model.getUpdatedProp(state, 'phiH2O');


% H2O dissolution factor
figure
plot(P/barsa, yH2O*1000);
title('yH2O*1000');

% CO2 dissolution factor
figure
plot(P/barsa, xCO2*100, '*-');
title('xCO2*100');

figure
plot(P/barsa, critind, '*-');
title('critind');

% Brine density

[rhopw, state] = model.getUpdatedProp(state, 'rhoPureWater');
[rhoc, state] = model.getUpdatedProp(state, 'rhoDissCO2');

figure
plot(P/barsa, rhopw, '*-');
title('water density');

% Phase densities

[rho, state] = model.getUpdatedProp(state, 'rho');
figure
plot(P/barsa, rho{1}, '*-');
title('water phase density')

figure
plot(P/barsa, rho{2}, '*-');
title('co2 phase density')



% figure
% plot(P/barsa, phiCO2);
% title('CO2 fugacity coefficient');

% figure
% plot(P/barsa, phiH2O);
% title('H2O fugacity coefficient');

% figure
% plot(P/barsa, ZG);
% title('compressibility factor');
