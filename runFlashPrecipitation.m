clear all
close all

mrstModule add compositional ad-core 

T0 = 273.15;
T = 30; % in degrees
T = T + T0;

model = CO2WaterSaltModel(T);

doplotgraph = true;
if doplotgraph
    gstyle = {'interpreter', 'none', 'LineWidth', 3, 'ArrowSize', 20, 'NodeFontSize', 14};
    [g, edgelabels] = setupGraph(model);
    figure
    h = plot(g, gstyle{:});
    return
end

p = (120 : 130)'*barsa;
mH2O = 100;
mCO2 = 50;
msalt  = 100;

n = size(p, 1);

pressure = p;
compmass{1} = mH2O*ones(n, 1);
compmass{2} = mCO2*ones(n, 1); 
compmass{3} = msalt*ones(n, 1); 

state.pressure = pressure;
state.compmass = compmass;

state = model.initiateState(state);

[s1 , state] = model.getUpdatedProp(state, 'phaseVolumes');

dp = 1e-5*barsa;

pressure = p + dp;
clear state
state.pressure = pressure;
state.compmass = compmass;

state = model.initiateState(state);

[s2 , state] = model.getUpdatedProp(state, 'phaseVolumes');

pAD = initVariablesADI(p);

pressure = pAD;
clear state
state.pressure = pressure;
state.compmass = compmass;

state = model.initiateState(state);

[sAD, state] = model.getUpdatedProp(state, 'phaseVolumes');

for i = 1 : 3
    dsAD{i} = diag(full(sAD{i}.jac{1}));
    ds{i} = (s2{i} - s1{i})/dp;
end

[ds{1} - dsAD{1}, ds{2} - dsAD{2}, ds{3} - dsAD{3}]
[s1{1} - sAD{1}.val, s1{2} - sAD{2}.val, s1{3} - sAD{3}.val]


%% phase index

figure
hold on
plot(msalt./mH2O, s{1})
plot(msalt./mH2O, s{2})
plot(msalt./mH2O, s{3})
