clear all
close all

mrstModule add compositional ad-core 

T0 = 273.15;
T = 30; % in degrees
T = T + T0;

molsalts = [0, 1, 2, 4]*mol;

model = DuanSunModel(T);

doplotgraph = true;
if doplotgraph
    gstyle = {'interpreter', 'none', 'LineWidth', 3, 'ArrowSize', 20, 'NodeFontSize', 14};
    [g, edgelabels] = setupGraph(model);
    figure
    h = plot(g, gstyle{:});
    title('Computational Graph')
end

for ind = 1 : numel(molsalts)
        
    msalt = molsalts(ind);
    clear state
    p = 600*(0.02 : 0.005 : 1)'*barsa;
    p = (10 : 1 : 80)'*barsa;
    
    state.pressure = p;
    state.molsalt = molsalts(ind)*ones(numel(p), 1);
    
    state = model.initiateState(state);

    [yH2O, state]    = model.getUpdatedProp(state, 'yH2O');
    [xCO2, state]    = model.getUpdatedProp(state, 'xCO2');
    [P, state]       = model.getUpdatedProp(state, 'pressure');
    [molsalt, state] = model.getUpdatedProp(state, 'molsalt');
    [nroot, state]   = model.getUpdatedProp(state, {'co2water', 'nroot'});
    [vG, state]      = model.getUpdatedProp(state, {'co2water', 'vG'});
    [vA, state]      = model.getUpdatedProp(state, {'co2water', 'vA'});
    [rhob, state]    = model.getUpdatedProp(state, 'rhoBrine');
    
    molH2O0 = model.physconst.molH2O0;
    vstoich = model.vstoich;
    vmolsalt = vstoich*molsalt;
    mCO2 = xCO2./(1 - xCO2).*(molH2O0 + vmolsalt);
        
    nfig = 2;
    figure(nfig)
    hold on
    plot(P/barsa, yH2O*1000);
    xlabel('Pressure / bar')
    title('yH2O*1000');
    nfig = nfig + 1;
    
    figure(nfig)
    hold on
    plot(P/barsa, mCO2, '*-');
    xlabel('Pressure / bar')
    title('CO2 mass fraction : mCO2');
    nfig = nfig + 1;
    
    % figure(nfig)
    % hold on
    % plot(P/barsa, nroot, '*-');
    % xlabel('Pressure / bar')
    % title('nroot');
    % nfig = nfig + 1;
    
    figure(nfig)
    hold on
    plot(P/barsa, vA, '*-');
    plot(P/barsa, vG, '*-');
    xlabel('Pressure / bar')
    title('Volume fractions: vA, vG');
    nfig = nfig + 1;
    
    figure(nfig)
    hold on
    plot(P/barsa, rhob, '*-');
    xlabel('Pressure / bar')
    ylabel('density / kg/m^3')
    title('Density brine');
    nfig = nfig + 1;
    
end

for ind = 2 : (nfig - 1)
    figure(ind)
    lgd = arrayfun(@(x) sprintf('%d', x), molsalts, 'uniformoutput', false);
    legend(lgd)
end


