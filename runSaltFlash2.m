clear all
close all

mrstModule add compositional ad-core

T0 = 273.15;
T = 30; % in degrees
T = T + T0;

model = CO2WaterSaltModel(T);
state = model.initiateState([]);

mCO2 = (40 : 0.1 : 60)';
n = size(mCO2, 1);

pressure = 100*barsa;
mH2O = 1000;
molsalt = 1*mol;

pressure = pressure*ones(n, 1);
compmass{1} = mH2O*ones(n, 1);
compmass{2} = mCO2; 
physconst = PhysicalConstants();
compmass{3} = physconst.molarmass('NaCl').*molsalt.*compmass{1};

clear state
state.pressure = pressure;
state.compmass = compmass;

state = model.initiateState(state);
state = model.computeFlash(state);

%% fetch values


[sA, state] = model.getUpdatedProp(state, 'sA');
[mA, state] = model.getUpdatedProp(state, 'mA');
[volA, state] = model.getUpdatedProp(state, 'volA');
[volG, state] = model.getUpdatedProp(state, 'volG');

p = model.getProp(state, 'pressure');
pind = model.getProp(state, 'phaseindex');

%% plotting

figure
plot(mCO2, sA)
title('Aqueous saturation');

figure
plot(mCO2, volA, '*-')
title('Volume of aqueous phase');

figure
plot(mCO2, volG, '*-')
title('Volume of CO2 rich phase');

figure
plot(mCO2, mA{1})
title('Water Mass Fraction in aqueous phase');

figure
plot(mCO2, mA{2}, '*-')
title('CO2 Mass Fraction in aqueous phase');

figure
plot(mCO2, mA{3})
title('salt Mass Fraction in aqueous phase');


[mG, state] = model.getUpdatedProp(state, 'mG');

figure
plot(mCO2, mG{1})
title('Water Mass Fraction in gas phase');

figure
plot(mCO2, mG{2}, '*-')
title('CO2 Mass Fraction in gas phase');


figure
hold on
plot(mCO2, mA{1})
plot(mCO2, mA{2})
plot(mCO2, mA{3})
plot(mCO2, mA{1} + mA{2} + mA{3})

figure
hold on
[rhoA, state] = model.getUpdatedProp(state, 'rhoA');
[rhoG, state] = model.getUpdatedProp(state, 'rhoG');
plot(mCO2, rhoA);
plot(mCO2, rhoG);
title('plot aqueous and gaseous density')

figure
plot(mCO2, pind)
title('Phase index Fraction');
