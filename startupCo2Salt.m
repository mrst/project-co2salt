% Startup mrst and geomec repo

rootdirname = fileparts(mfilename('fullpath'));
addpath(rootdirname);

% Add mrst modules

run(fullfile(rootdirname, 'mrst', 'mrst-core', 'startup'));

names = {'autodiff', 'visualization'};

names = cellfun(@(x) fullfile(ROOTDIR, '..', ['mrst-' x]), names, ...
                    'UniformOutput', false);

mrstPath('addroot', names{:});

% Add geomec specific modules
dirnames = {'project-multimodel', ...
            'transport'         , ...
            'models'            , ...
            'utils'};

for ind = 1 : numel(dirnames)
    dirname = fullfile(rootdirname, dirnames{ind});
    addpath(genpath(dirname));
end
