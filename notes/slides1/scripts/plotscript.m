clear all
close all


mrstModule add compositional ad-core

dosave = true;
savedir = '../figs';

T0 = 273.15;
T = 30; % in degrees
T = T + T0;

msalts = [0, 1, 2, 4]*mol;

set(0, 'DefaultAxesFontSize', 16);
set(0, 'DefaultLineLineWidth', 2);
for ind = 1 : numel(msalts)
    
    msalt = msalts(ind);
    model = DuanSunModel(T, msalt);
    clear state
    state.pressure = [0.1; 600*(0.02 : 0.02 : 1)']*barsa;
    state = model.initiateState(state);

    [yH2O, state] = model.getUpdatedProp(state, 'yH2O');
    [mCO2, state] = model.getUpdatedProp(state, 'mCO2');
    [P, state] = model.getUpdatedProp(state, 'pressure');

    figure(1)
    hold on
    plot(P/barsa, yH2O*1000);

    figure(2)
    hold on
    plot(P/barsa, mCO2);
end

%% plot

for ind = 1 : 2
    figure(ind)
    set(gcf, 'position', [217   220   701   368]);
    lgd = arrayfun(@(x) sprintf('%d mol', x), msalts, 'uniformoutput', false);
    legend(lgd, 'location', 'best')
    xlabel('Pressure (bar)')
    if ind == 1
        axis([0, 600, 0, 10])
        filename = 'waterinco2.png';
    else
        ylabel('mol');
        filename = 'co2inwater.png';
    end
    if dosave
        filename = fullfile(savedir, filename);
        saveas(gcf, filename);
    end        
end


