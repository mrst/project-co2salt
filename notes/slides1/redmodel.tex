\documentclass{sintefbeamer}
\usepackage{amsmath}
\usepackage{colortbl}
\usepackage{pifont}
\usepackage{xspace}
\usepackage{fancyvrb}
\usepackage[utf8]{inputenc}
\usepackage{chemformula}
\usepackage{siunitx}

\DeclareSIUnit\poise{P}

\usepackage[sorting=none]{biblatex}
\addbibresource{../refs.bib}

\def\eqsettings{\abovedisplayskip=1mm\belowdisplayskip=1mm}

\setbeamercovered{transparent}

\graphicspath{{./},{./figs/}}

%% Tikz
\usepackage{tikz}
\usetikzlibrary{arrows, positioning, calc, patterns, backgrounds, arrows.meta, matrix}

%% Improved boxes
\usepackage[most]{tcolorbox}
\newtcbox{\myLbox}[1][gray]{%
  left=1pt,right=4em,top=0mm,bottom=0mm,arc=0mm,height=.92\textheight,
  colback=#1!10!white,colframe=#1!10!white,frame hidden}
\newtcbox{\myRbox}[1][gray]{%
  left=2ex,right=2ex,top=1ex,bottom=4ex,arc=0mm,height=1.03\textheight,
  colback=#1!10!white,colframe=#1!10!white,frame hidden}
\tcbset{colback=red!5!white,colframe=red!75!black,left=3pt,right=3pt,top=3pt,bottom=3pt}

\usefonttheme{professionalfonts}
\setbeamertemplate{navigation symbols}{}
\setbeamerfont{block title}{size={},series=\bfseries}

\newcommand{\norm}[1]{\left\|#1\right\|}
\newcommand{\tens}[1]{\ensuremath{\boldsymbol{\mathsf{#1}}}}
\newcommand{\vect}[1]{\ensuremath{\boldsymbol{#1}}}
\newcommand{\mat} [1]{\ensuremath{\boldsymbol{#1}}}
\newcommand{\ddiv }  {\ensuremath{\mathtt{div}}}%
\newcommand{\dgrad}  {\ensuremath{\mathtt{grad}}}%
\newcommand{\davg}   {\ensuremath{\mathtt{avg}}}%
\newcommand{\vv}     {\ensuremath{\vect{v}}}
\newcommand{\vp}     {\ensuremath{\vect{p}}}
\newcommand{\vq}     {\ensuremath{\vect{q}}}
\newcommand{\ad}[1]  {\ensuremath{\left\langle #1 \right\rangle}}
\newcommand{\trans}{T}
\newcommand{\abs}[1]{\left| #1\right|}
\newcommand{\grad}{\nabla}
\newcommand{\dive}{\grad\cdot }
\newcommand{\Real}{\mathbb{R}}
\newcommand{\brac}[1]{\left<#1\right>}
\newcommand{\trace}{\mathrm{tr}}
\newcommand{\sym}{\ensuremath{\text{sym}}}

\newcommand{\myitemmark}{\raisebox{.75ex}{\scalebox{.6}{\protect\colorbox{gray}{\protect}}}}

\newenvironment{prettyList}
  {\begin{list}{\myitemmark}{\leftmargin=1.5em \itemindent=0em}}
  {\end{list}}
\newlength{\mylen}

\def\defcal #1{\expandafter\gdef\csname #1cal\endcsname{\mathcal{#1}}}
\def\defmat #1{\expandafter\def\csname #1mat\endcsname{\mat{#1}}\expandafter\def\csname #1tens\endcsname{\tens{#1}}}
\def\defmatsymb #1{\expandafter\def\csname #1mat\endcsname{\mat{\csname #1\endcsname}}\expandafter\def\csname #1tens\endcsname{\tens{\csname #1\endcsname}}}
\def\defvect #1{\expandafter\gdef\csname #1vec\endcsname{\vect{#1}}\expandafter\gdef\csname #1vect\endcsname{\vec{#1}}}
\def\defvectsymb #1{\expandafter\def\csname #1vec\endcsname{\vect{\csname #1\endcsname}}\expandafter\def\csname #1vect\endcsname{\vec{\csname #1\endcsname}}}

\defmat{A} \defmat{B} \defmat{S} \defmat{C} \defmat{U} \defmat{M} \defmat{D} \defmat{N}
\defmat{R} \defmat{T} \defmat{I} \defmat{K} \defmat{Q} \defmat{E} \defmat{Y} \defmat{P}
\defmat{Z} \defmat{L} \defmatsymb{Lambda} \defmatsymb{Delta} \defmatsymb{Omega}

\defvectsymb{alpha} \defvectsymb{pi} \defvectsymb{tau}
\defvect{e}  \defvect{c} \defvect{p} \defvect{u}
\defvect{n}  \defvect{v} \defvect{d} \defvect{y}  \defvect{x}
\defvect{q} \defvect{r}

\defcal{F} \defcal{S} \defcal{P} \defcal{I}
\def\sympos{\ensuremath{{\Scal\Pcal}} }

\def\eorth{e_{\text{orth}}}

\DeclareMathOperator{\var}{Var}
\DeclareMathOperator{\Expt}{E}

%% biblatex formatting

\DeclareNameFormat{short}{%
  \nameparts{#1}%
  \ifgiveninits
  {\usebibmacro{name:family-given}
    {\relax}
    {\relax}
    {\relax}
    {\relax}}
  {\usebibmacro{name:family-given}
    {\namepartfamily}
    {\relax}
    {\relax}
    {\relax}}%
  \usebibmacro{name:andothers}}

\def\shortcite #1{\begingroup\textcolor{blue}{\citename{#1}[short]{author}} (\citeyear{#1})\endgroup}
\def\shortcitetitle #1{\begingroup\footnotesize\textcolor{blue}{\it \citetitle{#1}} (\textcolor{blue}{\citename{#1}[short]{author}}, \citeyear{#1})\endgroup}
\def\mycite #1{\begingroup\textcolor{blue}{\citename{#1}[short]{author}: \footnotesize\textit{\citetitle{#1}}} (\citeyear{#1})\endgroup}

%% ---------------------------------------------------------------------
%% Title etc
\title[]{Reduced model for CO2-Salt-Water mixture}
\titlebackground{}
%% ----------------------------------------------
%% Colors and fonts
\setbeamercolor{uppercol}{fg=black,bg=gray!20}
\setbeamercolor{lowercol}{fg=black,bg=gray!5}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}

\maketitle

\begin{frame}
  \frametitle{Background}
  \begin{itemize}
  \item PCSAFT is computationally expensive.
  \item Simplified taylored models have been developed and are available in the literature.
  \item Easier to couple with transport.
  \item Easier to include kinetics.
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Reference work}
  We base our new implementation on 
  \begin{itemize}
  \item Paper sent be Aruoture which contains reference to kinetic model: \mycite{roels2014mu}. This paper uses models
    developed in the two following papers.
  \item \textbf{CO2-Water system} : \mycite{spycher2003co2}.
  \item \textbf{Salt and CO2-Water system} : \mycite{spycher2005co2}.
  \item In addition, there exists a THOUGH2 module \textbf{ECO2M} implemented by the authors of these papers:
    \mycite{pruess2011eco2m}.
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{\ch{CO2} - Water system}
  \begin{itemize}
  \item Two components (\ch{H2O} and \ch{CO2}) in two phases (aqueous and \ch{CO2}-rich phase)
  \item \textbf{Phase equilibrium equations}:
    \begin{align*}
       K_{\ch{H2O}} a_{\ch{H2O_{(aq)}}}&= f_{\ch{H2O_{(g)}}},&  K_{\ch{CO2_{(g)}}} a_{\ch{CO2_{(aq)}}}&= f_{\ch{CO2_{(g)}}} 
    \end{align*}
    where
    \begin{tabular}[t]{l@{: }l}
      fugacity & $f_{\ch{H2O_{(g)}}}$,  $f_{\ch{CO2_{(g)}}}$ \\
      activity & $a_{\ch{H2O_{(aq)}}}$,  $a_{\ch{CO2_{(aq)}}}$ 
    \end{tabular}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Model assumptions}
  \begin{itemize}
  \item The focus of the papers from Preuss et al is to obtain a model that can be used for coupled computation (same purpose as us!)
  \item It seems to me (but I am not an expert!) that they make a thorough comparison and are able to guarantee a good
    agreement of their approach with both experiments and other models. 
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Model assumptions : Liquid part}
  Model simplifications regarding the \textbf{water phase}
  \begin{enumerate}
  \item Approximation of equilibrium constants ($K_{\ch{H2O}}$ and $K_{\ch{CO2_{(g)}}}$) using \textit{average partial
      volumes}. This concerns only dependence in temperature.
  \item Henry's law for the $\ch{CO2}$ activity.
  \end{enumerate}
\end{frame}

\begin{frame}
  \frametitle{Model assumptions : Gas part}
  Model simplifications regarding the \textbf{CO2-rich phase}
  \begin{enumerate}
    \item<+-> Equation of state given by \textbf{Redlich-Kwong},
      \begingroup\tiny
      \begin{equation*}
        P = \left(\frac{RT}{V - b}\right) - \left(\frac{a}{T^{0.5}V(V + b)}\right)
      \end{equation*}
      \endgroup
      This is a cubic equation (in $V$) which is one of the \textbf{simplest}.
    \item<+-> For the computation of the fugacities, the \textbf{explicit dependence with respect to the composition is
        removed}. This brings a \textbf{significant} simplification in the overall flash computations (computation of
      the phase compositions), as all the expressions become fully explicit and can be computed \textbf{directly} (no
      non-linear equations have to be solved!).
    \item<+-> The change to \textbf{supercritical} state is done by replacing $K_{\ch{CO2_{(g)}}}$ with $K_{\ch{CO2_{(l)}}}$ when
      following conditions are met: Temperature below \SI{31}{\celsius} and gas volume below
      \SI{94}{\cubic\centi\meter\per\mol}. 
    \item<+-> The transition case of two \ch{CO2}-rich phase (gas and liquid) coexist is \textbf{excluded}.
    \end{enumerate}
\end{frame}

\begin{frame}
  \frametitle{Salt \ch{CO2} water system}
  \begin{itemize}
  \item One extra component: salt 
  \item The effect of salt is included by the introduction of an \textbf{activity coefficient} $\gamma$: In the phase equilibrium
    equation $K_{\ch{CO2_{(g)}}}$ is replaced by $\gamma K_{\ch{CO2_{(g)}}}$.
  \item Many models for the activity coefficient are considered: Duan and Sun (2003), Rumpf et al. (1994), Cramer (1982)/Battistelli
    et al. (1997), Drummond (1981), Nesbitt (1984).
  \item We implemented Duan and Sun, which also gives best experimental match.
  \end{itemize}
\end{frame}

\def\insertpic #1{\node{\includegraphics[width=0.4\textwidth]{#1}};}
\begin{frame}[fragile]
  \frametitle{First Validation}
  \begin{itemize}
  \item We reproduce the same results as the paper. Example at $T = \SI{30}{\celsius}$:
    \begin{center}
      \begin{tikzpicture}
        \matrix {&\node{Aqueous \ch{CO2}}; &\node{\ch{H2O} mol fraction};\\
          \node[rotate = 90]{paper}; &\insertpic{co2paper} & \insertpic{h2opaper} \\
          \node[rotate = 90]{
            \begin{minipage}[t]{3cm}
              \begin{center}
                own implementation
              \end{center}
            \end{minipage}}; &\insertpic{co2inwater} & \insertpic{waterinco2} \\};
      \end{tikzpicture}
    \end{center}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Flow properties}
  \begin{itemize}
  \item We use same setup as in ECO2M.
  \item Brine Viscosity from \shortcite{Phillips_1981}
    \begin{equation*}
      \frac{\mu}{\mu_w} = 1 + am + bm^2 + cm^3 + dT(1 - e^{km}).
    \end{equation*}
    We neglect CO2 content.
  \item CO2-rich phase viscosity is taken from standard tables, neglecting water content.
  \item We assume first constant values : at $P = \SI{120}{\bar}$ and $T = \SI{50}{\celsius}$,
    \begin{equation*}
      \mu_{\text{brine}} = \SI{0.5471}{\centi\poise},\quad \mu_{\text{CO2}} = \SI{0.0436}{\centi\poise}
    \end{equation*}
  \item We use standard Corey relative permability models.
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Salt Solubility}
  \begin{itemize}
  \item We use same setup as in ECO2M
  \item Equation from Potter, see \shortcite{chou1987phase}
    \begin{center}
      \includegraphics[width=0.5\textwidth]{solubility}
    \end{center}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Next steps}
  \begin{itemize}
  \item Implement precipitation
  \item We introduce the \textbf{kinetics}. We follow model from \shortcite{roels2014mu}
    \begin{equation*}
      Q_{l\to g}^{\ch{H2O}}(t) = k_{l\to g}^{\ch{H2O}} (\omega_g^{\ch{H2O},\max} - \omega_g^{\ch{H2O}}(t))
    \end{equation*}
    where
    \begin{center}
      \footnotesize
      \def\arraystretch{1.5}
      \begin{tabular}[h]{l@{: }l}
        $Q_{l\to g}^{\ch{H2O}}$& \ch{H2O} mass transfer from liquid to gas,\\
        $k_{l\to g}^{\ch{H2O}}$& Transfer coefficient,\\
        $\omega_g^{\ch{H2O},\max}$& Maximum (equilbirium) solubility,\\
        $\omega_g^{\ch{H2O}}(t)$& Current solubility (at current condition)
      \end{tabular}
    \end{center}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Comments}
  \begin{itemize}
  \item The model we use appears as parameter tuning - with a lot of insight.
  \item Opportunities:
    \begin{itemize}
    \item We can investigate the role of kinetics (at least qualitatively).
    \item We can investigate effect of capillary pressure (capillary suction at least qualitatively).
    \item We can include own data here to try to reproduce the lab experiments.
    \end{itemize}
  \item Very short time is left!
  \end{itemize}
\end{frame}

\end{document}


% Local Variables:
% eval: (defun LaTeX-item-beamer () (TeX-insert-macro "item") (delete-horizontal-space) (insert " ") (indent-according-to-mode))
% End:
