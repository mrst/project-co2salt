close all
clear all

load('simresults.mat')

savedir = '../figs';

x = model.G.cells.centroids(:, 1);

set(0, 'DefaultAxesFontSize', 16);
set(0, 'DefaultLineLineWidth', 3);

vnames = {'composition', 'saturation', 'massphaseAfraction', 'massphaseGfraction'};
for iname = 1 : numel(vnames)
    filename = vnames{iname};
    filename = sprintf('%s.avi', filename);
    filename = fullfile(savedir, filename);
    videos{iname} = VideoWriter(filename);
    videos{iname}.FrameRate = 3;
    open(videos{iname});
end
    
% composition figure
h = figure(1); set(h, 'Position', [676 60 601 895]);
% saturation figure
h = figure(2); set(h, 'Position', [676 60 601 895]);
% mass fraction in A phase figure
h = figure(3); set(h, 'Position', [182 112 1524 827]);
% mass fraction in G phase figure
h = figure(4); set(h, 'Position', [182 112 1524 827]);
        
for ind = 1 : numel(states)
    % for ind = 1 : 5
        
    state = states{ind};
    
    doPlotComposition = true;
    if doPlotComposition

        iname = 1;
        figure(iname);

        subplot(3, 1, 1)
        plot(x, state.core_compmass{1});
        axis([0, 1, 0, 810]);
        xlabel('x (meter)')
        ylabel('kg/m^3')
        title('H2O total density');

        subplot(3, 1, 2)
        plot(x, state.core_compmass{2});
        axis([0, 1, 0, 650]);
        xlabel('x (meter)')
        ylabel('kg/m^3')
        title('CO2 total density');
        
        subplot(3, 1, 3)
        plot(x, state.core_compmass{3});   
        axis([0, 1, 0, 190]);
        xlabel('x (meter)')
        ylabel('kg/m^3')
        title('NaCl total density');
        
        frame = getframe(gcf);
        writeVideo(videos{iname}, frame);
        
    end
    
    doPlotSaturation = true;
    if doPlotSaturation
        
        iname = 2;
        figure(iname);

        subplot(3, 1, 1)
        plot(x, state.core_s{1});
        axis([0, 1, 0, 1.1]);
        xlabel('x (meter)')
        title('Water-rich phase saturation');

        subplot(3, 1, 2)
        plot(x, state.core_s{2});
        axis([0, 1, 0, 1.1]);
        xlabel('x (meter)')
        title('CO2-rich phase saturation');
        
        subplot(3, 1, 3)
        plot(x, state.core_s{3});   
        axis([0, 1, 0, 0.03]);
        xlabel('x (meter)')
        title('solid phase saturation');
        
        frame = getframe(gcf);
        writeVideo(videos{iname}, frame);
    end
    
    doPlotWaterPhaseComposition = true;
    if doPlotWaterPhaseComposition
        
        iname = 3;
        figure(iname);

        subplot(3, 2, 1)
        plot(x, state.core_phaseMassFractions{1});
        axis([0, 1, 0, 1]);
        xlabel('x (meter)')
        title('H2O mass fraction in water phase');

        subplot(3, 2, 3)
        plot(x, state.core_phaseMassFractions{2});
        axis([0, 1, 0, 0.1]);
        xlabel('x (meter)')
        title('CO2 mass fraction in water phase');
        
        subplot(3, 2, 5)
        plot(x, state.core_phaseMassFractions{3});   
        axis([0, 1, 0, 1]);
        xlabel('x (meter)')
        title('NaCl mass fraction in water phase');
        
        subplot(3, 2, 2)
        plot(x, state.core_s{1});   
        axis([0, 1, 0, 1]);
        xlabel('x (meter)')
        title('Water phase saturation');
        
        frame = getframe(gcf);
        writeVideo(videos{iname}, frame);
    end
    
    
    doPlotGasPhaseComposition = true;
    if doPlotGasPhaseComposition

        iname = 4;
        figure(iname);

        subplot(2, 2, 1)
        plot(x, state.core_phaseMassFractions{4});
        axis([0, 1, 0, 0.05]);
        xlabel('x (meter)')
        title('H2O mass fraction in CO2-rich phase');

        subplot(2, 2, 3)
        plot(x, state.core_phaseMassFractions{5});
        axis([0, 1, 0, 1.1]);
        xlabel('x (meter)')
        title('CO2 mass fraction in CO2-rich phase');
        
        subplot(2, 2, 2)
        plot(x, state.core_s{2});   
        axis([0, 1, 0, 1.1]);
        xlabel('x (meter)')
        title('CO2-rich phase saturation');
        
        frame = getframe(gcf);
        writeVideo(videos{iname}, frame);
        
    end
    
end

for ind = 1 : numel(videos)
    close(videos{ind});
end

    