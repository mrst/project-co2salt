\documentclass{sintefbeamer}
\usepackage{amsmath}
\usepackage{colortbl}
\usepackage{pifont}
\usepackage{xspace}
\usepackage{fancyvrb}
\usepackage[utf8]{inputenc}
\usepackage{chemformula}
\usepackage{siunitx}
\usepackage{attachfile2}

\DeclareSIUnit\poise{P}
\DeclareSIUnit\darcy{D}
\DeclareSIUnit\barsa{bar}

\usepackage[sorting=none]{biblatex}
\addbibresource{../refs.bib}

\def\eqsettings{\abovedisplayskip=1mm\belowdisplayskip=1mm}

\setbeamercovered{transparent}

\graphicspath{{./},{./figs/}}

%% Tikz
\usepackage{tikz}
\usetikzlibrary{arrows, positioning, calc, patterns, backgrounds, arrows.meta, matrix}

%% Improved boxes
\usepackage[most]{tcolorbox}
\newtcbox{\myLbox}[1][gray]{%
  left=1pt,right=4em,top=0mm,bottom=0mm,arc=0mm,height=.92\textheight,
  colback=#1!10!white,colframe=#1!10!white,frame hidden}
\newtcbox{\myRbox}[1][gray]{%
  left=2ex,right=2ex,top=1ex,bottom=4ex,arc=0mm,height=1.03\textheight,
  colback=#1!10!white,colframe=#1!10!white,frame hidden}
\tcbset{colback=red!5!white,colframe=red!75!black,left=3pt,right=3pt,top=3pt,bottom=3pt}

\usefonttheme{professionalfonts}
\setbeamertemplate{navigation symbols}{}
\setbeamerfont{block title}{size={},series=\bfseries}

\newcommand{\norm}[1]{\left\|#1\right\|}
\newcommand{\tens}[1]{\ensuremath{\boldsymbol{\mathsf{#1}}}}
\newcommand{\vect}[1]{\ensuremath{\boldsymbol{#1}}}
\newcommand{\mat} [1]{\ensuremath{\boldsymbol{#1}}}
\newcommand{\ddiv }  {\ensuremath{\mathtt{div}}}%
\newcommand{\dgrad}  {\ensuremath{\mathtt{grad}}}%
\newcommand{\davg}   {\ensuremath{\mathtt{avg}}}%
\newcommand{\vv}     {\ensuremath{\vect{v}}}
\newcommand{\vp}     {\ensuremath{\vect{p}}}
\newcommand{\vq}     {\ensuremath{\vect{q}}}
\newcommand{\ad}[1]  {\ensuremath{\left\langle #1 \right\rangle}}
\newcommand{\trans}{T}
\newcommand{\abs}[1]{\left| #1\right|}
\newcommand{\grad}{\nabla}
\newcommand{\dive}{\grad\cdot }
\newcommand{\Real}{\mathbb{R}}
\newcommand{\brac}[1]{\left<#1\right>}
\newcommand{\trace}{\mathrm{tr}}
\newcommand{\sym}{\ensuremath{\text{sym}}}

\newcommand{\myitemmark}{\raisebox{.75ex}{\scalebox{.6}{\protect\colorbox{gray}{\protect}}}}

\newenvironment{prettyList}
  {\begin{list}{\myitemmark}{\leftmargin=1.5em \itemindent=0em}}
  {\end{list}}
\newlength{\mylen}

\def\defcal #1{\expandafter\gdef\csname #1cal\endcsname{\mathcal{#1}}}
\def\defmat #1{\expandafter\def\csname #1mat\endcsname{\mat{#1}}\expandafter\def\csname #1tens\endcsname{\tens{#1}}}
\def\defmatsymb #1{\expandafter\def\csname #1mat\endcsname{\mat{\csname #1\endcsname}}\expandafter\def\csname #1tens\endcsname{\tens{\csname #1\endcsname}}}
\def\defvect #1{\expandafter\gdef\csname #1vec\endcsname{\vect{#1}}\expandafter\gdef\csname #1vect\endcsname{\vec{#1}}}
\def\defvectsymb #1{\expandafter\def\csname #1vec\endcsname{\vect{\csname #1\endcsname}}\expandafter\def\csname #1vect\endcsname{\vec{\csname #1\endcsname}}}

\defmat{A} \defmat{B} \defmat{S} \defmat{C} \defmat{U} \defmat{M} \defmat{D} \defmat{N}
\defmat{R} \defmat{T} \defmat{I} \defmat{K} \defmat{Q} \defmat{E} \defmat{Y} \defmat{P}
\defmat{Z} \defmat{L} \defmatsymb{Lambda} \defmatsymb{Delta} \defmatsymb{Omega}

\defvectsymb{alpha} \defvectsymb{pi} \defvectsymb{tau}
\defvect{e}  \defvect{c} \defvect{p} \defvect{u}
\defvect{n}  \defvect{v} \defvect{d} \defvect{y}  \defvect{x}
\defvect{q} \defvect{r}

\defcal{F} \defcal{S} \defcal{P} \defcal{I}
\def\sympos{\ensuremath{{\Scal\Pcal}} }

\def\eorth{e_{\text{orth}}}

\DeclareMathOperator{\var}{Var}
\DeclareMathOperator{\Expt}{E}

%% biblatex formatting

\DeclareNameFormat{short}{%
  \nameparts{#1}%
  \ifgiveninits
  {\usebibmacro{name:family-given}
    {\relax}
    {\relax}
    {\relax}
    {\relax}}
  {\usebibmacro{name:family-given}
    {\namepartfamily}
    {\relax}
    {\relax}
    {\relax}}%
  \usebibmacro{name:andothers}}

\def\shortcite #1{\begingroup\textcolor{blue}{\citename{#1}[short]{author}} (\citeyear{#1})\endgroup}
\def\shortcitetitle #1{\begingroup\footnotesize\textcolor{blue}{\it \citetitle{#1}} (\textcolor{blue}{\citename{#1}[short]{author}}, \citeyear{#1})\endgroup}
\def\mycite #1{\begingroup\textcolor{blue}{\citename{#1}[short]{author}:\footnotesize\textit{\citetitle{#1}}} (\citeyear{#1})\endgroup}

%% ---------------------------------------------------------------------
%% Title etc
\title[]{Transport simulation using reduced model for  CO2-Water-NaCl mixture}
\titlebackground{}
%% ----------------------------------------------
%% Colors and fonts
\setbeamercolor{uppercol}{fg=black,bg=gray!20}
\setbeamercolor{lowercol}{fg=black,bg=gray!5}

\def\video {\includegraphics[width=0.1\textwidth]{video}}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}

\maketitle



\begin{frame}[fragile]
  \frametitle{Parameters}
  
  \def\tablegen{\begin{tabular}[h]{l@{: }l}
                  \multicolumn{2}{l}{General parameters}    \\
                  \hline
                  Core length           & \SI{1}{\meter}    \\
                  Temperature           & \SI{50}{\celsius} \\
                  Porosity              & 0.3               \\
                  Permeability          & \SI{0.1}{\darcy}  \\
                  Total simulation time & \SI{1}{\hour}\SI{22}{\minute}  
                \end{tabular}}
              
  \def\tablevisc{\begin{tabular}[h]{l@{: }l}
                   \multicolumn{2}{l}{Flow parameters}                      \\
                   \hline
                   Water phase viscosity        & \SI{0.5471}{\centi\poise} \\
                   CO2-rich phase viscosity     & \SI{0.0436}{\centi\poise} \\
                   Corey rel. perm. coefficient & 2 (both phases)           \\
                   residual saturations         & 0 (both phases)           \\
                   max rel. perm. value         & 1 (both phases)           \\
                   salt maximum solubility      & \SI{26}{\percent} 
                 \end{tabular}}
               
  \def\tableinit{\begin{tabular}[h]{l@{: }l}
                   \multicolumn{2}{l}{Initial composition}\\
                   \hline
                   Pressure  & \SI{120}{\barsa}\\
                   mass H2O  & \SI{81}{\percent} \\
                   mass CO2  & \SI{0}{\percent} \\
                   mass NaCl  & \SI{19}{\percent} (\SI{4}{\mol\per\kilo\gram}) 
                 \end{tabular}}
               
  \def\tableinput{\begin{tabular}[h]{l@{: }l}
                    \multicolumn{2}{l}{Injection composition}\\
                    \hline
                    Pressure  & \SI{130}{\barsa}\\
                    mass H2O  & \SI{0}{\percent} \\
                    mass CO2  & \SI{100}{\percent} \\
                    mass NaCl  & \SI{0}{\percent}
                  \end{tabular}}

  \begin{tikzpicture}
    \matrix[every node/.style = {anchor = north west}, ampersand replacement = \&, column sep = 1cm] {
      \node {\tablegen};\& \node {\tablevisc}; \\
      \node {\tableinit};\& \node {\tableinput}; \\
    };
  \end{tikzpicture}

\end{frame}

\def\insertvideo #1{
  \begin{tikzpicture}
    \matrix [anchor = south east, ampersand replacement = \&] {
      \node {\includegraphics[height=0.8\textheight]{#1.png}}; \& \node[inner ysep = 1cm] {\textattachfile{figs/#1.avi}{\includegraphics[width=0.1\textwidth]{video}}};\\
    };
  \end{tikzpicture}
  }

\begin{frame}[fragile]
  \frametitle{Simulation result : saturation}
  \begin{center}
   \insertvideo{saturation} 
  \end{center}
\end{frame}

\begin{frame}
  \frametitle{Simulation result : total density}
  \begin{center}
    \insertvideo{composition}
  \end{center}
\end{frame}


\begin{frame}
  \frametitle{Simulation result :  Mass Fractions in Water-rich phase}
  \begin{center}
    \insertvideo{massphaseAfraction}
  \end{center}
\end{frame}

\begin{frame}
  \frametitle{Simulation result :  Mass Fractions in CO2-rich phase}
  \begin{center}
    \insertvideo{massphaseGfraction}
  \end{center}
\end{frame}

\begin{frame}
  \frametitle{Reference literature}
  \begin{itemize}
  \item CO2-Water system : \mycite{spycher2003co2}.
  \item Salt and CO2-Water system : \mycite{spycher2005co2}.
  \item In addition, there exists a THOUGH2 module \textbf{ECO2M} implemented by the authors of these papers:
    \mycite{pruess2011eco2m}.
  \item We present now a short review of the modeling assumption
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{\ch{CO2} - Water system}
  \begin{itemize}
  \item Two components: \ch{H2O} and \ch{CO2}  
  \item Two phases: A water-rich and a \ch{CO2}-rich phases
  \item Phase equilibrium :
    \begin{align*}
      \ch{H2O_{(l)} <> H2O_{(g)}} && (K_{\ch{H2O}} &= \frac{f_{\ch{H2O_{(g)}}}}{a_{\ch{H2O_{(l)}}}}), & \ch{CO2_{(aq)} <> CO2_{(g)}} && (K_{\ch{CO2_{(g)}}} &= \frac{f_{\ch{CO2_{(g)}}}}{a_{\ch{CO2_{(aq)}}}})
    \end{align*}
    where
    \begin{tabular}[t]{l@{: }l}
      fugacity & $f_{\ch{H2O_{(g)}}}$,  $f_{\ch{CO2_{(g)}}}$ \\
      activity & $a_{\ch{H2O_{(g)}}}$,  $a_{\ch{CO2_{(g)}}}$ 
    \end{tabular}
  \item The \ch{CO2}-rich phase can be a gas-like or a liquid-like (subcritical \ch{CO2}).
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Model assumptions : Liquid part}
  Model simplifications regarding the \textbf{water phase}
  \begin{enumerate}
  \item \ch{H2O} activity equal to one.
  \item Approximation of equilibrium constants ($K_{\ch{H2O}}$ and $K_{\ch{CO2_{(g)}}}$) using \textit{average partial
      volumes}. This concerns only dependence in temperature.
  \item Henry's law for the $\ch{CO2}$ activity.
  \end{enumerate}
\end{frame}

\begin{frame}
  \frametitle{Model assumptions : Gas part}
  Model simplifications regarding the \textbf{CO2-rich phase}
  \begin{enumerate}
  \item Equation of state given by \textbf{Redlich-Kwong},
    \begingroup\tiny
    \begin{equation*}
      P = \left(\frac{RT}{V - b}\right) - \left(\frac{a}{T^{0.5}V(V + b)}\right)
    \end{equation*}
    \endgroup
    This is a cubic equation (in $V$) which is one of the \textbf{simplest}.
  \item \textbf{Standard} mixing rules for the computation of $a$ and $b$ (see paper).
  \item For the computation of the fugacities, the \textbf{explicit dependence with respect to the composition is
      removed}. This brings a \textbf{significant} simplification in the overall flash computations (computation of
    the phase compositions), as all the expressions become fully explicit and can be computed \textbf{directly} (no
    non-linear equations have to be solved).
  \item The change to \textbf{subcritical} state is done by replacing $K_{\ch{CO2_{(g)}}}$ with $K_{\ch{CO2_{(l)}}}$ when
    following conditions are met: Temperature below \SI{31}{\celsius} and gas volume below
    \SI{94}{\cubic\centi\meter\per\mol}. The transition case of both \ch{CO2}-rich phase (gas and liquid) coexist is
    \textbf{excluded} (it is said that it is typically very small).
  \end{enumerate}
\end{frame}

\begin{frame}
  \frametitle{Salt \ch{CO2} water system}
  \begin{itemize}
  \item One extra component: salt 
  \item The effect of salt is included by the introduction of an \textbf{activity coefficient} $\gamma$ (In the phase equilibrium
    equation $K_{\ch{CO2_{(g)}}}$ is replaced by $\gamma K_{\ch{CO2_{(g)}}}$)
  \item Many models for the activity coefficient are considered: Duan and Sun (2003), Rumpf et al. (1994), Cramer (1982)/Battistelli
    et al. (1997), Drummond (1981), Nesbitt (1984).
  \item We implemented Duan and Sun, which also gives best experimental match.
    \vspace*{5mm}
    
    \tiny\abovedisplayskip=1mm\belowdisplayskip=1mm\jot=0pt
    In Duan and Sun, the activity coefficient $\gamma$ is given by
    \begin{align*}
      \log(\gamma) &= 2\lambda(m_{\ch{Na}} + m_{\ch{K}} + 2m_{\ch{Ca}} + 2m_{\ch{Mg}}) + \xi((m_{\ch{Na}} + m_{\ch{K}} +
                     m_{\ch{Ca}} + m_{\ch{Mg}})) - 0.07m_{\ch{SO4}}
    \end{align*}
    with
    \begin{align*}
      \lambda &= -\num{0.411370585} + \num{6.07632013e-4}T + \num{97.5347708}/T  - \num{0.0237622469} P/T  \\
              &\quad\quad + \num{0.0170656236}P/(630 - T) + \num{1.41335834e-5} T\log(P)\\
      \xi &= \num{3.36389723e-4} - \num{1.98298980e-5}T + \num{2.12220830e-3}P/T - \num{5.24873303e-3}P/(630 - T)
    \end{align*}
    \footnotesize (just to give you an example of what kind of models we are talking about : interpolation and curve fitting!)
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Flow properties}
  \begin{itemize}
  \item We use same setup as in ECO2M.
  \item Brine Viscosity from \shortcite{Phillips_1981}
    \begin{equation*}
      \frac{\mu}{\mu_w} = 1 + am + bm^2 + cm^3 + dT(1 - e^{km}).
    \end{equation*}
    We neglect CO2 content.
  \item CO2-rich phase viscosity is taken from standard tables, neglecting water content.
  \item We assume first constant values : at $P = \SI{120}{\bar}$ and $T = \SI{50}{\celsius}$,
    \begin{equation*}
      \mu_{\text{brine}} = \SI{0.5471}{\centi\poise},\quad \mu_{\text{CO2}} = \SI{0.0436}{\centi\poise}
    \end{equation*}
  \item We use standard Corey relative permability models.
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Salt Solubility}
  \begin{itemize}
  \item We use same setup as in ECO2M
  \item Equation from Potter, see \shortcite{chou1987phase}
    \begin{center}
      \includegraphics[width=0.5\textwidth]{solubility}
    \end{center}
  \end{itemize}
\end{frame}

\end{document}


% Local Variables:
% eval: (defun LaTeX-item-beamer () (TeX-insert-macro "item") (delete-horizontal-space) (insert " ") (indent-according-to-mode))
% End:
