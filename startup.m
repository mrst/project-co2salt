dirnames = {'.', 'project-multimodel', 'transport'};

for idir = 1 : numel(dirnames)
    addpath(dirnames{idir});
end
