classdef KineticCoreModel < CompositeModel
% Core model 2 phase
    
    properties

        rock
        fluid

        kineticConstants
        
        nC  = 3; % number of components (H2O, CO2, salt)
        nP  = 3; % number of phases (A, G, S)
        nFP = 2; % number of fluid phases (A, G)
        T
        
        TC = 31 + 273.15 % Critical temperature of pure CO2

        physconst
        compmolarmass
        
    end
   
    methods
        
        function model = KineticCoreModel(G, rock, fluid, T)
            
            model = model@CompositeModel('kinetic');
            
            c = 1e5;
            model.kineticConstants = {c, c, c, c, c, c};
            % model.kineticConstants = {0, 0, 0, 0, 0, 0};
            
            model.G = G;
            model.rock = rock;
            model.fluid = fluid;
            
            model.operators = setupOperatorsTPFA(G, rock);

            submodel = CO2WaterSaltModel(T);
            submodel.modelname = 'equilibrium';
            submodel = submodel.setAlias({'pressure', VarName({'..'}, 'pressure')});
            submodel = submodel.setAlias({'compmass', VarName({'..'}, 'compmass', 3, (1 : 3))});
            names = submodel.names;
            [~, lia] = ismember({'pressure', 'compmass'}, names);
            names(lia) = [];
            submodel.names = names;
            
            model.SubModels{1} = submodel;
            
            %% Declare model variable names
            
            names = {'phaseCompMass'     , ... % Phase Mass 
                     'pressure'          , ... % Pressure
                     's'                 , ... % saturations
                     'kr'                , ... % Phase relative permeabilities
                     'mu'                , ... % Phase viscosities
                     'mob'               , ... % Phase mobilities
                     'phaseFlux'         , ... % Phase fluxes
                     'phaseMassFractions', ... % Phase Mass fraction
                     'compmass'          , ... % Component Mass (= component density)
                     'kineticTerms'      , ... % Kinetic source terms
                     'rho'               , ... % phase densities
                     'phaseFlags'        , ... % A (aqueous) G (co2-rich) S (solid)
                     'phaseCompMassFlux' , ... % Phase Mass
                     'phaseMassEqs'      , ... % Equations for total phase mass that defines implicitly the saturations
                     'volumeCons'        , ... % Equation for volume conservation
                    };
            
            model.names = horzcat(model.names, names);
            
            nP  = model.nP;
            nFP = model.nFP;
            nC  = model.nC;
            
            vardims = model.vardims;
            
            vardims('phaseCompMass') = 6;
            % phaseCompMass{1} : H2O in A
            % phaseCompMass{2} : CO2 in A
            % phaseCompMass{3} : salt in A
            % phaseCompMass{4} : H2O in G
            % phaseCompMass{5} : CO2 in G
            % phaseCompMass{6} : salt in S
            
            
            vardims('s') = nP;

            vardims('kr')  = nFP;
            vardims('mu')  = nFP;
            vardims('mob') = nFP;
            vardims('rho') = nFP;
            vardims('compmass') = nC;
            
            vardims('phaseFlux') = nFP;
            
            vardims('phaseCompMassFlux') = 5;
            % phaseCompMassFlux{1} : H2O in A
            % phaseCompMassFlux{2} : CO2 in A
            % phaseCompMassFlux{3} : salt in A
            % phaseCompMassFlux{4} : H2O in G
            % phaseCompMassFlux{5} : CO2 in G
            
            vardims('phaseMassFractions') = 5;
            % phaseMassFractions{1} : H2O in A
            % phaseMassFractions{2} : CO2 in A
            % phaseMassFractions{3} : salt in A
            % phaseMassFractions{4} : H2O in G
            % phaseMassFractions{5} : CO2 in G
            
            vardims('phaseFlags') = nP;
            
            vardims('kineticTerms') = 6;
            
            vardims('phaseMassEqs') = nP;
            
            model.vardims = vardims;
            
            %% Declare primary variable names
            
            pnames = {'pressure', ... 
                      'phaseCompMass1', ...
                      'phaseCompMass2', ...
                      'phaseCompMass3', ...
                      'phaseCompMass4', ...
                      'phaseCompMass5', ...
                      'phaseCompMass6', ...
                      'sA', ...
                      'sG', ...
                      'sS'};
            model.pnames = pnames;
            
            %% setup aliases
            aliases = {{'muA', VarName({'.'}, 'mu', nP, 1)}, ...
                       {'muG', VarName({'.'}, 'mu', nP, 2)}, ...
                       {'krA', VarName({'.'}, 'kr', nP, 1)}, ...
                       {'krG', VarName({'.'}, 'kr', nP, 2)}, ...
                       {'muA', VarName({'.'}, 'mu', nP, 1)}, ...
                       {'muG', VarName({'.'}, 'mu', nP, 2)}, ...
                       {'phaseCompMassA', VarName({'.'}, 'phaseCompMass', 6, (1 : 3))}, ...
                       {'phaseCompMassG', VarName({'.'}, 'phaseCompMass', 6, (4 : 5))}, ...
                       {'phaseCompMassS', VarName({'.'}, 'phaseCompMass', 6, 6)}, ...
                       {'sA', VarName({'.'}, 's', nP, 1)}, ...
                       {'sG', VarName({'.'}, 's', nP, 2)}, ...
                       {'sS', VarName({'.'}, 's', nP, 3)}, ...
                       {'mA', VarName({'.'}, 'phaseMassFractions',  5, (1 : 3))}, ...  %  aqueous phase
                       {'mG', VarName({'.'}, 'phaseMassFractions',  5, (4 : 5))}, ...  %  gas/Co2-rich phase
                       {'vA', VarName({'.'}, 'phaseFlux', nP, 1)}, ...
                       {'vG', VarName({'.'}, 'phaseFlux', nP, 2)}, ...
                       {'flagA', VarName({'.'}, 'phaseflags', nP, 1)}, ... 
                       {'flagG', VarName({'.'}, 'phaseflags', nP, 2)}, ...
                       {'flagS', VarName({'.'}, 'phaseflags', nP, 3)}, ...
                       {'phaseCompMass1', VarName({'.'}, 'phaseCompMass', 6, 1)}, ...
                       {'phaseCompMass2', VarName({'.'}, 'phaseCompMass', 6, 2)}, ...
                       {'phaseCompMass3', VarName({'.'}, 'phaseCompMass', 6, 3)}, ...
                       {'phaseCompMass4', VarName({'.'}, 'phaseCompMass', 6, 4)}, ...
                       {'phaseCompMass5', VarName({'.'}, 'phaseCompMass', 6, 5)}, ...
                       {'phaseCompMass6', VarName({'.'}, 'phaseCompMass', 6, 6)}};
            model.aliases = horzcat(model.aliases, aliases);

            %% setup propfunctions
            
            % update kr
            fn = @(model_, state_) computeRelPerms(model_, state_);
            inputnames = {'sA', 'sG'};
            model = model.addPropFunction('kr', fn, inputnames, {'.'}); %
            
            % update mu
            fn = @(model_, state_) computeViscosities(model_, state_);
            inputnames = {'pressure'};
            model = model.addPropFunction('mu', fn, inputnames, {'.'}); %
                                                                        
            % update mob
            fn = @(model_, state_) computeMobilities(model_, state_);
            inputnames = {'kr', 'mu'};
            model = model.addPropFunction('mob', fn, inputnames, {'.'});
                                                                         
            % update phaseFlux
            fn = @(model, state) model.computePhaseFluxes(state);
            inputnames = {'pressure', 'mob'};
            model = model.addPropFunction('phaseFlux', fn, inputnames, {'.'});

            % update phaseMassFractions and compmass
            fn = @(model, state) model.computePhaseMassFractions(state);
            inputnames = {'phaseCompMass'};
            model = model.addPropFunction('phaseMassFractions', fn, inputnames, {'.'});
            model = model.addPropFunction('compmass', fn, inputnames, {'.'});
            model = model.addPropFunction('phaseFlags', fn, inputnames, {'.'});
            
            % update kineticTerms
            fn = @(model, state) model.computeKineticSrcTerms(state);
            inputnames = {'phaseCompMass', ...
                          VarName({'equilibrium'}, 'phaseCompMass')};
            model = model.addPropFunction('kineticTerms', fn, inputnames, {'.'}); %

            % update rho
            fn = @(model, state) model.computeDensities(state);
            inputnames = {VarName({'equilibrium', 'twophases', 'co2water'}, 'vG'), ...
                          VarName({'equilibrium', 'twophases', 'co2water'}, 'rhoDissCO2'), ...
                          VarName({'equilibrium', 'twophases'}, 'rhoBrine'), ...
                          'phaseFlags', ...
                          'mA', ...
                          'phaeCompMassG'};
            model = model.addPropFunction('rho', fn, inputnames, {'.'}); %
            
            % update phaseCompMassFlux 
            fn = @(model, state) model.computePhaseMassFluxes(state);
            inputnames = {'phaseFlux', 'rho', 'phaseMassFractions', 'phaseFlags'};
            model = model.addPropFunction('phaseCompMassFlux', fn, inputnames, {'.'}); %
            
            % update phaseMassEqs            
            fn = @(model, state) model.assemblePhaseMassEqs(state);
            inputnames = {'s', 'rho', 'phaseCompMass', 'phaseFlags'};
            model = model.addPropFunction('phaseMassEqs', fn, inputnames, {'.'});
            
            % update volumeCons
            fn = @(model, state) model.assembleVolumeCons(state);
            inputnames = {'s'};
            model = model.addPropFunction('volumeCons', fn, inputnames, {'.'});
                        
            model = model.initiateCompositeModel;

        end
        

        function state = computePhaseFluxes(model, state)
            
            op = model.operators;
            T = op.T;
            up = op.faceUpstr;
           
            [p, state] = model.getUpdatedProp(state, 'pressure');
            [mob, state] = model.getUpdatedProp(state, 'mob');
            
            kgrad = op.T.*op.Grad(p);
            
            nFP = model.nFP;
            v = cell(1, nFP);
            for ind = 1 : nFP
                fmob = up(kgrad < 0, mob{ind});
                v{ind} = -fmob.*kgrad;
            end
            
            state = model.setProp(state, 'phaseFlux', v);
            
        end
        
        function state = computePhaseMassFractions(model, state)
            
            [pcm, state] = model.getUpdatedProp(state, 'phaseCompMass');
            [adsample, doAD] = getSampleAD(pcm{:});
            n = numelValue(pcm{1});
            
            % initialisation of phaseFlags
            for ip = 1 : model.nP
                phaseFlags{ip} = false(n, 1);
            end
            
            % initialisation of phaseMassFractions (pmf)
            for ind = 1 : 5
                pmf{ind} = double2ADI(nan(n, 1), adsample);
            end
            
            mA = pcm{1} + pcm{2} + pcm{3};
            mG = pcm{4} + pcm{5};
            mS = pcm{6};
            
            indA = (mA > eps);
            if any(indA)
                pmf{1}(indA) = pcm{1}(indA)./mA(indA);
                pmf{2}(indA) = pcm{2}(indA)./mA(indA);
                pmf{3}(indA) = pcm{3}(indA)./mA(indA);
                phaseFlags{1}(indA) = true;
            end
            
            indG = (mG > eps);
            if any(indG)
                pmf{4}(indG) = pcm{4}(indG)./mG(indG);
                pmf{5}(indG) = pcm{5}(indG)./mG(indG);
                phaseFlags{2}(indG) = true;
            end

            indS = (mS > eps);
            if any(indS)
                phaseFlags{3}(indS) = true;
            end            
            
            cm{1} = pcm{1} + pcm{4};
            cm{2} = pcm{2} + pcm{5};
            cm{3} = pcm{3} + pcm{6};
            
            state = model.setProp(state, 'phaseMassFractions', pmf);
            state = model.setProp(state, 'compmass', cm);
            state = model.setProp(state, 'phaseFlags', phaseFlags);
            
        end
        
        function state = computeKineticSrcTerms(model, state)

            [pcm, state] = model.getUpdatedProp(state, 'phaseCompMass');
            [pcmeq, state] = model.getUpdatedProp(state, {'equilibrium', 'phaseCompMass'});
            
            kc = model.kineticConstants;
            
            % H2O exchange terms
            sc1 = - kc{1}*(pcm{1} - pcmeq{1});
            sc2 = kc{4}*(pcm{4} - pcmeq{4});
            
            sc{1} = (sc1 + sc2);
            sc{4} = -sc{1};
            
            % CO2 exchange terms
            sc1 = - kc{2}*(pcm{2} - pcmeq{2});
            sc2 = kc{5}*(pcm{5} - pcmeq{5});
            
            sc{2} = (sc1 + sc2);
            sc{5} =  -sc{2};
        
            % Salt exchange terms
            sc1 = - kc{3}*(pcm{3} - pcmeq{3});
            sc2 = kc{6}*(pcm{6} - pcmeq{6});

            sc{3} = (sc1 + sc2);            
            sc{6} =  -sc{3};
            
            state = model.setProp(state, 'kineticTerms', sc);
            
        end
        
        function state = computeDensities(model, state)
            
            [rhoG, state] = model.getUpdatedProp(state, {'equilibrium', 'twophases', 'rhoG'});
            [mA, state] = model.getUpdatedProp(state, 'mA');
            [pmG, state] = model.getUpdatedProp(state, 'phaseCompMassG');
            
            [pflags, state] = model.getUpdatedProp(state, 'phaseFlags');
            
            % We use brine density as computed at equilibrium
            [rhob, state] = model.getUpdatedProp(state, {'equilibrium', 'twophases', 'rhoBrine'});
            % We use dissolved co2 density as computedd in CO2WaterModel (depends only from pressure)
            [rhoc, state] = model.getUpdatedProp(state, {'equilibrium', 'twophases', 'co2water', 'rhoDissCO2'});
            
            % Aqueous phase density, see ref2 section 3.1
            invrhoA = (mA{1} + mA{3})./rhob + mA{2}./rhoc;
            rhoA = 1./invrhoA;
            rhoA(~pflags{1}) = NaN;
                        
            % CO2 rich phase density
            rhoG(~pflags{2}) = NaN;
            
            rhoS = model.SubModels{1}.rhosalt*pflags{3};
            
            rho = {rhoA, rhoG, rhoS};
            state = model.setProp(state, 'rho', rho);
    
        end

        function state = computePhaseMassFluxes(model, state)
            
            [v, state] = model.getUpdatedProp(state, 'phaseFlux');
            [rho, state] = model.getUpdatedProp(state, 'rho');
            [pmf, state] = model.getUpdatedProp(state, 'phaseMassFractions');
            [pflags, state] = model.getUpdatedProp(state, 'phaseFlags');
            
            up = model.operators.faceUpstr;
            nFP = model.nFP;
            nC = model.nC;
           
            for ip = 1 : nFP % phase index
                flag = up(v{ip} > 0, pflags{ip});
                for ic = 1 : nC % component index
                    icp = (ip - 1)*nC + ic;
                    if icp < 6
                        pmv{icp} = up(v{ip} > 0, rho{ip}.*pmf{icp}).*v{ip};
                        pmv{icp}(~flag) = 0;
                    end
                end
            end
            
            state = model.setProp(state, 'phaseCompMassFlux', pmv);
        
        end
        
        function state = assemblePhaseMassEqs(model, state)
            
            [s, state] = model.getUpdatedProp(state, 's');
            [rho, state] = model.getUpdatedProp(state, 'rho');
            [pcm, state] = model.getUpdatedProp(state, 'phaseCompMass');
            [pflags, state] = model.getUpdatedProp(state, 'phaseFlags');
            
            pv = model.operators.pv;
            dt = state.dt;
            
            % saturaton equation for A
            eqs{1} = dt*pv.*((pcm{1} + pcm{2} + pcm{3}) - rho{1}.*s{1});
            eqs{2} = dt*pv.*((pcm{4} + pcm{5}) - rho{2}.*s{2});
            eqs{3} = dt*pv.*(pcm{6} - rho{3}.*s{3});
            
            for ip = 1 : model.nP
                eqs{ip}(~pflags{ip}) = s{ip}(~pflags{ip});
            end
                        
            state = model.setProp(state, 'phaseMassEqs', eqs);
            
        end
        

        function state = assembleVolumeCons(model, state)
            
            [s, state] = model.getUpdatedProp(state, 's');
            
            pv = model.operators.pv;
            dt = state.dt;
            vc = 1 - (s{1} + s{2} + s{3});
            
            state = model.setProp(state, 'volumeCons', vc);
            
        end

    end
    
end

