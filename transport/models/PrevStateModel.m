classdef PrevStateModel < ComponentModel
% Transport Salt CO2 model.
    
    properties
        nP  = 3; % number of phase
        nFP = 2; % number of fluid phases
        nC  = 3; % number of components
    end
    
    methods
        
        function model = PrevStateModel()
            
            model = model@ComponentModel('prevstate');


            %% declare model variable names
            
            names = {'pressure', ... % Pressure
                     'compmass', ...
                    };
            
            model.names = names;

            nC = model.nC;
            vardims = model.vardims;
            vardims('compmass') = nC;
            model.vardims = vardims;
            
            %% setup aliases
            
            aliases = {{'mH2O' , VarName({'.'}, 'compmass', nC, 1)}, ...
                       {'mCO2' , VarName({'.'}, 'compmass', nC, 2)}, ...
                       {'msalt', VarName({'.'}, 'compmass', nC, 3)}, ...
                      };
            model.aliases = aliases;
            
        end
        
    end
    
end

