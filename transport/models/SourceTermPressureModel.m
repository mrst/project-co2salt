classdef SourceTermPressureModel < CO2WaterSaltModel
% Transport Salt CO2 model.

    properties
        fluid;
    end
   
    methods
        
        function model = SourceTermPressureModel(fluid, T)
            
            model = model@CO2WaterSaltModel(T);
            model.modelname = 'bcpress';
            
            model.fluid = fluid;

            %% Declare model variable names
            
            names = {'kr'      , ... % Phase relative permeabilities
                     'mu'      , ... % Phase viscosities
                     'mob'     , ... % Phase mobilities
                    };
            model.names = horzcat(model.names, names);

            nFP = model.nFP;
            vardims = model.vardims;
            vardims('kr')  = nFP;
            vardims('mu')  = nFP;
            vardims('mob') = nFP;
            model.vardims = vardims;
            
            %% setup aliases
            
            aliases = {{'krA' , VarName({'.'}, 'kr' , nFP, 1)}, ...
                       {'krG' , VarName({'.'}, 'kr' , nFP, 2)}, ...
                       {'muA' , VarName({'.'}, 'mu' , nFP, 1)}, ...
                       {'muG' , VarName({'.'}, 'mu' , nFP, 2)}, ...
                      };
            model.aliases = horzcat(model.aliases, aliases);

            %% setup propfunctions
            
                        
            fn = @(model, state) computeRelPerms(model, state);
            inputnames = {'sA', 'sG'};
            model = model.addPropFunction('kr', fn, inputnames, {'.'}); %
            
            fn = @(model, state) computeMobilities(model, state);
            inputnames = {'kr', 'mu'};
            model = model.addPropFunction('mob', fn, inputnames, {'.'}); %

            fn = @(model, state) computeViscosities(model, state);
            inputnames = {'pressure'};
            model = model.addPropFunction('mu', fn, inputnames, {'.'}); %

            
        end
        
    end
    
end

