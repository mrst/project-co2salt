classdef KinPrevStateModel < ComponentModel
% Transport Salt CO2 model.
    

    methods
        
        function model = KinPrevStateModel()
            
            model = model@ComponentModel('prevstate');


            %% declare model variable names
            
            names = {'phaseCompMass'};
            model.names = names;

            vardims = model.vardims;
            vardims('phaseCompMass') = 6;
            model.vardims = vardims;
            
        end
        
    end
    
end

