classdef KinTransportModel < CompositeModel
% Transport 2 phase

    properties
        nC  = 3; % number of components (H2O, CO2, salt)
        nP  = 3; % number of phases (A, G, S)
        nFP = 2; % number of fluid phases (A, G)
    end
    
    methods
        
        function model = KinTransportModel(G, rock, fluid, bc, T)
           
            model = model@CompositeModel('transport');
            
            model.G = G;
            
            names = {'massCons', ...   % mass conservation equations
                     'srcterms', ...   % Source terms (positive = source, negative = sink)
                     'time', ...       % time (required by simulateScheduleAD)
                     'dt'};
            model.names = names;

            nC = model.nC;
            vardims = model.vardims;
            vardims('massCons') = 6;
            % massCons{1} : mass conservation equation for H2O in A
            % massCons{2} : mass conservation equation for CO2 in A
            % massCons{3} : mass conservation equation for salt in A
            % massCons{4} : mass conservation equation for H2O in G
            % massCons{5} : mass conservation equation for CO2 in G
            % massCons{6} : mass conservation equation for salt in S
            vardims('srcTerms') = 6;
            model.vardims = vardims;
            
            fn = @(model, state) model.setupMassCons(state);
            inputnames = {'srcterms', 'dt', ...
                          VarName({'core'}, 'phaseCompMass'), ...
                          VarName({'core'}, 'phaseCompMassFlux'), ...
                          VarName({'prevstate'}, 'phaseCompMass')};
            model = model.addPropFunction('massCons', fn, inputnames, {'.'}); %

            %% setup prevstate model
            
            model.SubModels = {};
            model.SubModels{end + 1} = KinPrevStateModel();
            model.SubModels{end + 1} = KineticCoreModel(G, rock, fluid, T);
            model.SubModels{end + 1} = SourceTermPressureModel(fluid, T);
            
            model.SubModels{2}.modelname = 'core';
            
            %% setup propfunctions for the coupling
            
            bcmodel = model.getAssocModel({'bcpress'});
            coremodel = model.getAssocModel({'core'});

            bcfaces = bc.face;
            bccells = sum(G.faces.neighbors(bcfaces, :), 2);
            nbc = numel(bcfaces);
            
            cellToBCMap = sparse((1 : nbc)', bccells, 1, nbc, G.cells.num);
            BCTocellMap = cellToBCMap';
            T = coremodel.operators.T_all(bcfaces);
            
            bcdata = struct('cellToBCMap', cellToBCMap, ...
                            'BCTocellMap', BCTocellMap, ...
                            'T'          , T          , ...
                            'pressure'   , bc.pressure);
            bcdata.compmass = bc.compmass; % compmass is a cell and cannot be assigned using struct function

            fn = @(model_, state_) setupKinSourceTerm(model_, state_, bcdata);
            inputnames = {VarName({'bcpress'}, 'phaseMassFractions'), ...
                          VarName({'bcpress'}, 'rho'), ...
                          VarName({'bcpress'}, 'mob'), ...
                          VarName({'core'}, 'pressure'), ...
                          VarName({'core'}, 'phaseMassFractions'), ...
                          VarName({'core'}, 'rho'), ...
                          VarName({'core'}, 'mob')};
            model = model.addPropFunction('srcterms', fn, inputnames, {'.'}); 
                     
            %% initiate composite model
            model = model.initiateCompositeModel();
            
        end

        function [problem, state] = getEquations(model, state0, state, dt, forces, varargin)
            
            opt = struct('Verbose'    , mrstVerbose,...
                         'reverseMode', false      ,...
                         'resOnly'    , false      ,...
                         'iteration'  , -1);
            opt = merge_options(opt, varargin{:});

            stateAD = model.initStateAD(state);

            coremodel = model.getAssocModel({'core'});
            bcmodel   = model.getAssocModel({'bcpress'});
            prevmodel = model.getAssocModel({'prevstate'});
            
            %% setup time variables
            
            stateAD = model.setProp(stateAD, 'dt', dt);

            %% setup previous state
            
            pm0 = model.getProp(state0, {'core', 'phaseCompMass'});
            stateAD = model.setProp(stateAD, {'prevstate', 'phaseCompMass'}, pm0);

            %% assemble equations
            
            %% mass conservation equations
            [masscons, stateAD] = model.getUpdatedProp(stateAD, 'massCons');
            
            %% volume conservation equations
            [volumecons, stateAD] = model.getUpdatedProp(stateAD, {'core', 'volumeCons'});
                        
            %% volume conservation equations
            [phasemasseqs, stateAD] = model.getUpdatedProp(stateAD, {'core', 'phaseMassEqs'});
                        
            %% concatenate the two conservation equations
            eqs = horzcat(masscons, {volumecons}, phasemasseqs);
            
            %% setup linearized problem
            
            primaryVars = model.getModelPrimaryVarNames();

            [names, types] = deal(cell(1, 10));
            names{1} = 'cons H2O in A';
            names{2} = 'cons CO2 in A';
            names{3} = 'cons salt in A';
            names{4} = 'cons H2O in G';
            names{5} = 'cons CO2 in G';
            names{6} = 'cons salt in S';
            names{7} = 'volume conservation';
            names{8} = 'mass A';
            names{9} = 'mass G';
            names{10} = 'mass S';
            [types{:}] = deal('cell');
            
            problem = model.setupLinearizedProblem(eqs, types, names, primaryVars, state, dt);
            
        end

        function [state, report] = updateState(model, state, problem, dx, forces)
            [state, report] = updateState@CompositeModel(model, state, problem, dx, forces);
            capnames = {'phaseCompMass1', ...
                        'phaseCompMass2', ...
                        'phaseCompMass3', ...
                        'phaseCompMass4', ...
                        'phaseCompMass5', ...
                        'phaseCompMass6', ...
                        'sA', ...
                        'sG', ...
                        'sS', ...
                        'pressure'};
            capnames = {'sA', ...
                        'sG', ...
                        'sS', ...
                        'pressure'};
            for ind = 1 : numel(capnames)
                % prop = model.getProp(state, {'core', capnames{ind}});
                % if any(prop < 0)
                    % fprintf('\n%s negative\n', capnames{ind});
                % end
                state = model.capProperty(state, {'core', capnames{ind}}, 0);
            end
            
        end
        
        
        function forces = getValidDrivingForces(model)
            
            forces = getValidDrivingForces@CompositeModel(model);
            forces.W   = [];
            forces.src = [];
            forces.bc  = [];
            
        end
        
        function state = setupMassCons(model, state)
            
            coremodel = model.getAssocModel({'core'});
            op = coremodel.operators;
            pv = op.pv;
            
            [srcterms, state] = model.getUpdatedProp(state, 'srcterms');
            [dt, state] = model.getUpdatedProp(state, 'dt');
            
            [cm, state] = model.getUpdatedProp(state, {'core', 'phaseCompMass'});
            [u, state] = model.getUpdatedProp(state, {'core', 'phaseCompMassFlux'});
            [ksrc, state] = model.getUpdatedProp(state, {'core', 'kineticTerms'});
            
            [cm0, state] = model.getUpdatedProp(state, {'prevstate', 'phaseCompMass'});
            
            nC = model.nC;
            nFP = model.nFP;
            
            for ip = 1 : nFP
                for ic = 1 : nC
                    icp = (ip - 1)*nC + ic;
                    massCons{icp} = pv.*(cm{icp} - cm0{icp}) - dt*pv.*ksrc{icp};
                    if icp < 6
                        massCons{icp} = massCons{icp} + dt*(op.Div(u{icp}) - srcterms{icp});
                    end
                end
            end            
            
            state = model.setProp(state, 'massCons', massCons);
            
        end
        
        function [state, report] = updateAfterConvergence(model, state0, state, dt, drivingForces)
            [state, report] = updateAfterConvergence@ComponentModel(model, state0, state, dt, drivingForces);
        end
        
    end
    
end

