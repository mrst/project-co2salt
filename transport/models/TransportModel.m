classdef TransportModel < CompositeModel
% Transport 2 phase

    properties
        nC  = 3; % number of components (H2O, CO2, salt)
        nP  = 3; % number of phases (A, G, S)
        nFP = 2; % number of fluid phases (A, G)
    end
    
    methods
        
        function model = TransportModel(G, rock, fluid, bc, T)
            
            model = model@CompositeModel('transport');
            
            model.G = G;
            
            names = {'massCons', ...   % mass conservation equations
                     'volumeCons', ... % volume conservation equations
                     'srcterms', ...   % Source terms (positive = source, negative = sink)
                     'time', ...       % time (required by simulateScheduleAD)
                     'dt'};
            model.names = names;

            nC = model.nC;
            vardims = model.vardims;
            vardims('massCons') = nC;
            vardims('srcTerms') = nC;
            model.vardims = vardims;
            
            
            fn = @(model, state) model.setupMassCons(state);
            inputnames = {'srcterms', 'dt', ...
                          VarName({'core'}, 'massflux'), ...
                          VarName({'core'}, 'compmass'), ...
                          VarName({'prevstate'}, 'compmass')};
            model = model.addPropFunction('massCons', fn, inputnames, {'.'}); %

            fn = @(model, state) model.setupVolumeCons(state);
            inputnames = {VarName({'core'}, 'phaseVolumes')};
            model = model.addPropFunction('volumeCons', fn, inputnames, {'.'}); %

            
            %% setup prevstate model
            
            model.SubModels = {};
            model.SubModels{end + 1} = PrevStateModel();
            model.SubModels{end + 1} = CoreModel(G, rock, fluid, T);
            model.SubModels{end + 1} = SourceTermPressureModel(fluid, T);
            
            %% setup propfunctions for the coupling
            
            bcmodel = model.getAssocModel({'bcpress'});
            coremodel = model.getAssocModel({'core'});

            bcfaces = bc.face;
            bccells = sum(G.faces.neighbors(bcfaces, :), 2);
            nbc = numel(bcfaces);
            
            cellToBCMap = sparse((1 : nbc)', bccells, 1, nbc, G.cells.num);
            BCTocellMap = cellToBCMap';
            T = coremodel.operators.T_all(bcfaces);
            
            bcdata = struct('cellToBCMap', cellToBCMap, ...
                            'BCTocellMap', BCTocellMap, ...
                            'T'          , T          , ...
                            'pressure'   , bc.pressure);
            bcdata.compmass = bc.compmass; % compmass is a cell and cannot be assigned using struct function

            fn = @(model_, state_) setupSourceTerm(model_, state_, bcdata);
            inputnames = {VarName({'bcpress'}, 'phaseMassFractions'), ...
                          VarName({'bcpress'}, 'rho'), ...
                          VarName({'bcpress'}, 'mob'), ...
                          VarName({'core'}, 'pressure'), ...
                          VarName({'core'}, 'phaseMassFractions'), ...
                          VarName({'core'}, 'rho'), ...
                          VarName({'core'}, 'mob')};
            model = model.addPropFunction('srcterms', fn, inputnames, {'.'}); 
                     
            %% initiate composite model
            model = model.initiateCompositeModel();
            
        end


        function [problem, state] = getEquations(model, state0, state, dt, forces, varargin)
            
            opt = struct('Verbose'    , mrstVerbose,...
                         'reverseMode', false      ,...
                         'resOnly'    , false      ,...
                         'iteration'  , -1);
            opt = merge_options(opt, varargin{:});

            stateAD = model.initStateAD(state);

            coremodel = model.getAssocModel({'core'});
            bcmodel   = model.getAssocModel({'bcpress'});
            prevmodel = model.getAssocModel({'prevstate'});
            
            %% setup time variables
            
            stateAD = model.setProp(stateAD, 'dt', dt);

            %% setup previous state
            
            cm0 = coremodel.getProp(state0, 'compmass');
            stateAD = prevmodel.setProp(stateAD, 'compmass', cm0);

            %% assemble equations
            
            %% mass conservation equations
            [masscons, stateAD] = model.getUpdatedProp(stateAD, 'massCons');
            
            %% volume conservation equations
            [volumecons, stateAD] = model.getUpdatedProp(stateAD, 'volumeCons');
                        
            %% concatenate the two conservation equations
            eqs = horzcat(masscons, {volumecons});
            
            %% setup linearized problem
            
            primaryVars = model.getModelPrimaryVarNames();

            nC = model.nC;
            [names, types] = deal(cell(1, nC + 1));
            names{1} = 'water mass conservation';
            names{2} = 'co2 mass conservation';
            names{3} = 'salt mass conservation';
            names{4} = 'volume conservation';
            [types{:}] = deal('cell');
            
            problem = model.setupLinearizedProblem(eqs, types, names, primaryVars, state, dt);
            
        end

        function [state, report] = updateState(model, state, problem, dx, forces)
            [state, report] = updateState@CompositeModel(model, state, problem, dx, forces);
            state = model.capProperty(state, {'core', 'mH2O'}, 0);
            state = model.capProperty(state, {'core', 'mCO2'}, 0);
            state = model.capProperty(state, {'core', 'msalt'}, 0);
            state = model.capProperty(state, {'core', 'pressure'}, 0);
        end
        
        
        function forces = getValidDrivingForces(model)
            
            forces = getValidDrivingForces@CompositeModel(model);
            forces.W   = [];
            forces.src = [];
            forces.bc  = [];
            
        end

        function state = setupVolumeCons(model, state)
            
            nP = model.nP;
            phasevols = model.getUpdatedProp(state, {'core', 'phaseVolumes'});
            volumecons = phasevols{1};
            for ind = 2 : nP
                volumecons = volumecons + phasevols{ind};
            end
            volumecons = volumecons - 1;
            
            state = model.setProp(state, 'volumeCons', volumecons);
            
        end
        
        function state = setupMassCons(model, state)
            
            model0 = model.getAssocModel({'prevstate'});
            coremodel = model.getAssocModel({'core'});
            
            op = coremodel.operators;
            pv = op.pv;
            
            [srcterms, state] = model.getUpdatedProp(state, 'srcterms');
            [dt, state] = model.getUpdatedProp(state, 'dt');

            [u, state] = coremodel.getUpdatedProp(state, 'massflux');
            [cm, state] = coremodel.getUpdatedProp(state, 'compmass');
            
            [cm0, state] = model0.getUpdatedProp(state, 'compmass');
            
            nC = model.nC;
            massCons = cell(1, nC);
            
            for ind = 1 : nC
                acc = pv.*(cm{ind} - cm0{ind});
                massCons{ind} = acc + dt*(op.Div(u{ind}) - srcterms{ind});
            end            
            
            state = model.setProp(state, 'massCons', massCons);
            
        end
        
        function [state, report] = updateAfterConvergence(model, state0, state, dt, drivingForces)
            [state, report] = updateAfterConvergence@ComponentModel(model, state0, state, dt, drivingForces);
            state = model.updateProp(state, {'core', 'compmass'});
            state = model.updateProp(state, {'core', 'phaseMassFractions'});
            state = model.updateProp(state, {'core', 'flagS'});
        end
        
    end
    
end

