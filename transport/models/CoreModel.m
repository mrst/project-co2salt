classdef CoreModel < CO2WaterSaltModel
% Core model 2 phase
    
    properties
        rock
        fluid
    end
   
    methods
        
        function model = CoreModel(G, rock, fluid, T)
            
            model = model@CO2WaterSaltModel(T);
            model.modelname = 'core';
            
            model.G = G;
            model.rock = rock;
            model.fluid = fluid;
            
            model.operators = setupOperatorsTPFA(G, rock);

            %% Declare model variable names
            
            names = {'kr', ...        % Phase relative permeabilities
                     'mu', ...        % Phase viscosities
                     'mob', ...       % Phase mobilities
                     'phaseflux', ... % Phase fluxes
                     'massflux', ...  % Component mass fluxes
                    };
            model.names = horzcat(model.names, names);
            
            nP  = model.nP;
            nFP = model.nFP;
            nC  = model.nC;
            
            vardims = model.vardims;
            vardims('kr')  = nFP;
            vardims('mu')  = nFP;
            vardims('mob') = nFP;
            vardims('phaseflux') = nFP;
            vardims('massflux') = nC;
            model.vardims = vardims;
            
            %% Declare primary variable names
            
            pnames = {'pressure', 'mH2O', 'mCO2', 'msalt'};
            model.pnames = pnames;
            
            %% setup aliases
            
            aliases = {{'muA', VarName({'.'}, 'mu', nP, 1)}, ...
                       {'muG', VarName({'.'}, 'mu', nP, 2)}, ...
                       {'krA', VarName({'.'}, 'kr', nP, 1)}, ...
                       {'krG', VarName({'.'}, 'kr', nP, 2)}, ...
                       {'vA', VarName({'.'}, 'phaseflux', nP, 1)}, ...
                       {'vG', VarName({'.'}, 'phaseflux', nP, 2)}, ...
                      };
            model.aliases = horzcat(model.aliases, aliases);

            %% setup propfunctions
            
            fn = @(model_, state_) computeRelPerms(model_, state_);
            inputnames = {'sA', 'sG'};
            model = model.addPropFunction('kr', fn, inputnames, {'.'}); %
            
            fn = @(model_, state_) computeMobilities(model_, state_);
            inputnames = {'kr', 'mu'};
            model = model.addPropFunction('mob', fn, inputnames, {'.'}); %

            fn = @(model_, state_) computeViscosities(model_, state_);
            inputnames = {'pressure'};
            model = model.addPropFunction('mu', fn, inputnames, {'.'}); %
            
            fn = @(model, state) model.computePhaseFluxes(state);
            inputnames = {'pressure', 'mob'};
            model = model.addPropFunction('phaseflux', fn, inputnames, {'.'}); %

            fn = @(model, state) model.computeMassFluxes(state);
            inputnames = {'phaseflux', 'rho', 's', 'phaseMassFractions'};
            model = model.addPropFunction('massflux', fn, inputnames, {'.'}); %

        end
        

        function state = computePhaseFluxes(model, state)
            
            op = model.operators;
            T = op.T;
            up = op.faceUpstr;
           
            [p, state] = model.getUpdatedProp(state, 'pressure');
            [mob, state] = model.getUpdatedProp(state, 'mob');
            
            kgrad = op.T.*op.Grad(p);
            
            nFP = model.nFP;
            v = cell(1, nFP);
            for ind = 1 : nFP
                fmob = up(kgrad < 0, mob{ind});
                v{ind} = -fmob.*kgrad;
            end
            
            state = model.setProp(state, 'phaseflux', v);
            
        end

        
        function state = computeMassFluxes(model, state)
            
            [v, state]   = model.getUpdatedProp(state, 'phaseflux');
            [rho, state] = model.getUpdatedProp(state, 'rho');
            [s, state] = model.getUpdatedProp(state, 's');
            [m, state]   = model.getUpdatedProp(state, 'phaseMassFractions');
            
            up = model.operators.faceUpstr;
            zeroAD = model.AutoDiffBackend.convertToAD(zeros(numelValue(v{1}), 1), v{1});
            
            nC = model.nC;
            nFP = model.nFP;
            
            ipc = 1;
            for ic = 1 : nC
                u{ic} = zeroAD;
                for ip = 1 : nFP
                    ipc = nC*(ip - 1) + ic;
                    if ipc < 6
                        % value of upwind mass fraction multiplied by density
                        mc = up(v{ip} > 0, rho{ip}.*m{ipc});
                        % value of upwind saturation
                        sup = up(v{ip} > 0, s{ip});
                        ind = (value(sup) == 0);
                        if any(ind)
                            % if the phase is absent, the phase mass fraction are ill-defined (nan). We set those values to zero.
                            mc(ind) = 0;
                        end
                        u{ic} = u{ic} + mc.*v{ip}; % salt is special
                    end
                end
            end
            
            dodebug = false;
            if dodebug
                tol = 1e-10;
                test = testconsistency(model, state);

                if isa(test, 'ADI')
                    if any(abs(test.val) > tol)
                        warning('value test failed')
                    end
                    
                    if any(abs(test.jac{1}) > tol)
                        warning('derivattive test failed')
                    end
                else
                    if any(test > tol)
                        warning('value test failed')
                    end                
                end
            end

            state = model.setProp(state, 'massflux', u);
            
        end

        
        function state = computeSrcTerms(model, state, bcdata)

            BCTocellMap = bcdata.BCTocellMap;
            
            bcmodel = model.getAssocModel({'bcpress'});
            [ubc, state] = bcmodel.getUpdatedProp(state, 'u');
            
            for i = 1 : 2
               srcterms{i} = BCTocellMap*ubc{i};
            end
            
            state = model.setProp(state, 'srcterms', srcterms);
            
        end


    end
    
end

