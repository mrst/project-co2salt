clear all
close all

mrstModule add compositional ad-core ad-blackoil ad-props mrst-gui 

T0 = 273.15;
T = 50; % in degrees
T = T + T0;

p0 = 120*barsa;
dp = 10*barsa;

G = cartGrid([100 1], [1 1]);
G = computeGeometry(G);

rock = makeRock(G, 100*milli*darcy, 0.3);

fluid = initSimpleADIFluid('phases','WO',            ... % Fluid phase: water
                           'mu', [1, 1]*centi*poise, ... % Viscosity (this values are not used - we give them just to avoid warning)
                           'n', [2, 2]               ... % Surface density [kg/m^3]
                           );

model = KineticCoreModel(G, rock, fluid, T);

doplotgraph = true;
if doplotgraph
    gstyle = {'interpreter', 'none', 'LineWidth', 3, 'ArrowSize', 20, 'NodeFontSize', 14};
    [g, edgelabels] = setupGraph(model, 'resolveIndex', false);
    figure
    h = plot(g, gstyle{:});
    
    f = fopen('test.sif', 'w');
    edges = g.Edges;
    edges = edges.Variables;
    for ind = 1 : size(edges, 1)
        fprintf(f, '%s x %s\n', edges{ind, 1}, edges{ind, 2});
    end
    fclose(f);
    return
end


% Use equilibrium model to setup boundary conditions and initial state

compmodel = CO2WaterSaltModel(T);

nP = compmodel.nP;
nC = compmodel.nC;

%% Left part of the domain : CO2 

pressure = p0 + dp;

mH2O    = 0; % mass of water
mCO2    = 100; % mass of co2
molsalt = 0*mol/kilogram; % molality

physconst = PhysicalConstants();
msalt = physconst.molarmass('NaCl').*molsalt.*mH2O;

compmass{1} = mH2O;
compmass{2} = mCO2;
compmass{3} = msalt;

% normalize composition (switch to total densities for the given pressure)
state = compmodel.initiateState([]);
state = compmodel.setProp(state, 'compmass', compmass);
state = compmodel.setProp(state, 'pressure', pressure);

[pvols, state] = compmodel.getUpdatedProp(state, 'phaseVolumes');
vol = 0;
for ind = 1 : nP
   vol = vol + pvols{ind};
end
for ind = 1 : nC
    compmass{ind} = compmass{ind}./vol;
end

pressure_left = pressure;
compmass_left = compmass;


%% Right part of the domain : brine with some CO2

pressure = p0;

mH2O     = 1000; % mass of water
mCO2     = 0; % mass of co2
molsalt  = 4*mol/kilogram; % molality

physconst = PhysicalConstants();
msalt = physconst.molarmass('NaCl').*molsalt.*mH2O;

compmass{1} = mH2O;
compmass{2} = mCO2;
compmass{3} = msalt;

% normalize composition (switch to total densities for the given pressure)
state = compmodel.initiateState([]);
state = compmodel.setProp(state, 'compmass', compmass);
state = compmodel.setProp(state, 'pressure', pressure);

[pvols, state] = compmodel.getUpdatedProp(state, 'phaseVolumes');
vol = 0;
for ind = 1 : nP
   vol = vol + pvols{ind};
end
for ind = 1 : nC
    compmass{ind} = compmass{ind}./vol;
end

pressure_right = pressure;
compmass_right = compmass;

%% setup initial state

initcase = 'left';
switch initcase
  case 'middle'
    nc = G.cells.num;
    leftnc = floor(nc/2);
    rightnc = nc - leftnc;

    pressure_res = [pressure_left*ones(leftnc, 1); pressure_right*ones(rightnc, 1)];
    for ind = 1 : 3
        compmass_res{ind} = [compmass_left{ind}*ones(leftnc, 1); compmass_right{ind}*ones(rightnc, 1)];    
    end
    
  case 'left'
    nc = G.cells.num;

    pressure_res = pressure_right*ones(nc, 1);
    for ind = 1 : 3
        compmass_res{ind} = compmass_right{ind}*ones(nc, 1);    
    end
    
end

state = compmodel.initiateState([]);
state = compmodel.setProp(state, 'pressure', pressure_res);
state = compmodel.setProp(state, 'compmass', compmass_res);

[pcm, state] = compmodel.getUpdatedProp(state, 'phaseCompMass');
[s, state] = compmodel.getUpdatedProp(state, 's');

state = model.initiateState([]);

state = model.setProp(state, 'phaseCompMass', pcm);
state = model.setProp(state, 's', s);
state = model.setProp(state, 'pressure', pressure_res);

state = model.updateProp(state, 'kr');
state = model.updateProp(state, 'mu');
state = model.updateProp(state, 'mob');
state = model.updateProp(state, 'phaseFlux');
state = model.updateProp(state, 'phaseMassFractions');
state = model.updateProp(state, 'compmass');
state = model.updateProp(state, 'kineticTerms');
state = model.updateProp(state, 'rho');
state = model.updateProp(state, 'phaseCompMassFlux');
state = model.updateProp(state, 'phaseMassEqs');
state = model.updateProp(state, 'volumeCons');




