function state = computeRelPerms(model, state)
    
    fluid = model.fluid;

    [sW, state] = model.getUpdatedProp(state, 'sA');
    [sO, state] = model.getUpdatedProp(state, 'sG');

    krW = fluid.krW(sW);
    krO = fluid.krO(sO);
    
    state = model.setProp(state, 'krA', krW);
    state = model.setProp(state, 'krG', krO);
    
end
