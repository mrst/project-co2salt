function state = setupSourceTerm(model, state, bcdata)
    
    nP = model.nP;
    nC = model.nC;
    nFP = model.nFP;
    
    cellToBCMap = bcdata.cellToBCMap;
    BCTocellMap = bcdata.BCTocellMap;
    T           = bcdata.T;
    p_bc        = bcdata.pressure;
    compmass_bc = bcdata.compmass;
    
    bcmodel   = model.getAssocModel({'bcpress'});
    coremodel = model.getAssocModel({'core'});
    
    state = bcmodel.setProp(state, 'pressure', p_bc);
    state = bcmodel.setProp(state, 'compmass', compmass_bc);
    
    [m_bc, state]   = bcmodel.getUpdatedProp(state, 'phaseMassFractions');
    [rho_bc, state] = bcmodel.getUpdatedProp(state, 'rho');
    [mob_bc, state] = bcmodel.getUpdatedProp(state, 'mob');
    
    [p_res, state]   = coremodel.getUpdatedProp(state, 'pressure');
    [m_res, state]   = coremodel.getUpdatedProp(state, 'phaseMassFractions');
    [rho_res, state] = coremodel.getUpdatedProp(state, 'rho');
    [mob_res, state] = coremodel.getUpdatedProp(state, 'mob');
    
    p_res = cellToBCMap*p_res;
    for i = 1 : nFP
        rho_res{i} = cellToBCMap*rho_res{i};
        mob_res{i} = cellToBCMap*mob_res{i};
    end
    
    for i = 1 : 5
        m_res{i} = cellToBCMap*m_res{i};
    end
    
    nbc = numelValue(p_bc);
    zeroAD = @(dim) model.AutoDiffBackend.convertToAD(zeros(dim, 1), p_res);

    % Treat pressure BC
    dP = (p_bc - p_res);
    
    % Determine if pressure bc are injecting or producing
    injDir = (dP > 0);
    
    for ic = 1 : nC
        
        u{ic} = zeroAD(nbc);
        
        if any(~injDir)
            % pressure drives flow outwards
            subs = ~injDir;
            q = zeroAD(nnz(subs));
            for ip = 1 : nFP
                ipc = nC*(ip - 1) + ic;
                if ipc < 6
                    dq = rho_res{ip}(subs).*m_res{ipc}(subs).*mob_res{ip}(subs).*T(subs).*dP(subs);
                    % handle case where phase is not present
                    pind = (value(mob_res{ip}(subs)) == 0);
                    if any(pind)
                        dq(pind) = 0;
                    end
                    q = q + dq;
                end
            end

            u{ic}(subs) = q;
        end

        if any(injDir)
            % In this case, pressure drives flow inwards, we get the injection rate determined by the sat field
            subs = injDir;
            q = zeroAD(nnz(subs));
            for ip = 1 : nFP
                ipc = nC*(ip - 1) + ic;
                if ipc < 6
                    dq = rho_bc{ip}(subs).*m_bc{ipc}(subs).*mob_bc{ip}(subs).*T(subs).*dP(subs);
                    % handle case where phase is not present
                    pind = (value(mob_bc{ip}(subs)) == 0);
                    if any(pind)
                        dq(pind) = 0;
                    end
                    q = q + dq;
                end
            end
            u{ic}(subs) = q;
        end
    
        srcterms{ic} = BCTocellMap*u{ic};
    end
    
    state = model.setProp(state, 'srcterms', srcterms);
    
end
