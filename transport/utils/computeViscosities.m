function state = computeViscosities(model, state)
%    @techreport{Phillips_1981,
%	doi = {10.2172/6301274},
%	url = {https://doi.org/10.2172%2F6301274},
%	year = 1981,
%	month = {jun},
%	publisher = {Office of Scientific and Technical Information  ({OSTI})},
%	author = {S.L. Phillips and A. Igbene and J.A. Fair and H. Ozbek and M. Tavana},
%	title = {Technical databook for geothermal energy utilization}
%}
    
    p = model.getProp(state, 'pressure');
    if isa(p, 'ADI')
        n = numelValue(p);
    else
        n = numel(p);
    end
    
    % Value from Phillips_1981 for zero salt concentration and temperature = 50 celsius
    muA = 0.5471*centi*poise;
    % Value from MRST CO2lab (see plotPropFromCO2lab script)
    muG = 0.0436*centi*poise;
    
    state = model.setProp(state, 'muA', muA*ones(n, 1));
    state = model.setProp(state, 'muG', muG*ones(n, 1));
    
end
