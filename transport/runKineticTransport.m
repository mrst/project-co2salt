clear all
close all

mrstModule add compositional ad-core ad-blackoil ad-props mrst-gui 

T0 = 273.15;
T = 50; % in degrees
T = T + T0;

p0 = 120*barsa;
dp = 10*barsa;

G = cartGrid([100 1], [1 1]);
G = computeGeometry(G);

rock = makeRock(G, 100*milli*darcy, 0.3);

fluid = initSimpleADIFluid('phases','WO',            ... % Fluid phase: water
                           'mu', [1, 1]*centi*poise, ... % Viscosity (this values are not used - we give them just to avoid warning)
                           'n', [2, 2]               ... % Surface density [kg/m^3]
                           );

compmodel = CO2WaterSaltModel(T);

nP = compmodel.nP;
nC = compmodel.nC;

%% Left part of the domain : CO2 

pressure = p0 + dp;

mH2O    = 0; % mass of water
mCO2    = 100; % mass of co2
molsalt = 0*mol/kilogram; % molality

physconst = PhysicalConstants();
msalt = physconst.molarmass('NaCl').*molsalt.*mH2O;

compmass{1} = mH2O;
compmass{2} = mCO2;
compmass{3} = msalt;

% normalize composition (switch to total densities for the given pressure)
state = compmodel.initiateState([]);
state = compmodel.setProp(state, 'compmass', compmass);
state = compmodel.setProp(state, 'pressure', pressure);

[pvols, state] = compmodel.getUpdatedProp(state, 'phaseVolumes');
vol = 0;
for ind = 1 : nP
   vol = vol + pvols{ind};
end
for ind = 1 : nC
    compmass{ind} = compmass{ind}./vol;
end

pressure_left = pressure;
compmass_left = compmass;


%% Right part of the domain : brine with some CO2

pressure = p0;

mH2O     = 1000; % mass of water
mCO2     = 0; % mass of co2
molsalt  = 4*mol/kilogram; % molality

physconst = PhysicalConstants();
msalt = physconst.molarmass('NaCl').*molsalt.*mH2O;

compmass{1} = mH2O;
compmass{2} = mCO2;
compmass{3} = msalt;

% normalize composition (switch to total densities for the given pressure)
state = compmodel.initiateState([]);
state = compmodel.setProp(state, 'compmass', compmass);
state = compmodel.setProp(state, 'pressure', pressure);

[pvols, state] = compmodel.getUpdatedProp(state, 'phaseVolumes');
vol = 0;
for ind = 1 : nP
   vol = vol + pvols{ind};
end
for ind = 1 : nC
    compmass{ind} = compmass{ind}./vol;
end

pressure_right = pressure;
compmass_right = compmass;

%% Setup boundary condition

bc = pside([], G, 'LEFT',  pressure);
leftfaces = bc.face;
leftnf = numel(leftfaces);

bc = pside([], G, 'RIGHT', pressure);
rightfaces = bc.face;
rightnf = numel(rightfaces);

faces = [leftfaces; rightfaces];
pressure_bc = [pressure_left*ones(leftnf, 1); pressure_right*ones(rightnf, 1)];
for ind = 1 : 3
    compmass_bc{ind} = [compmass_left{ind}*ones(leftnf, 1); compmass_right{ind}*ones(rightnf, 1)];    
end

clear bc
bc.face = faces;
bc.pressure = pressure_bc;
bc.compmass = compmass_bc;

%% initiate transport model

model = KinTransportModel(G, rock, fluid, bc, T);
model.nonlinearTolerance = 1e-9; 

doplotgraph = false;
if doplotgraph
    gstyle = {'interpreter', 'none', 'LineWidth', 3, 'ArrowSize', 20, 'NodeFontSize', 14};
    [g, edgelabels] = setupGraph(model, 'resolveIndex', false);
    figure
    h = plot(g, gstyle{:});
    
    f = fopen('test.sif', 'w');
    edges = g.Edges;
    edges = edges.Variables;
    for ind = 1 : size(edges, 1)
        fprintf(f, '%s x %s\n', edges{ind, 1}, edges{ind, 2});
    end
    fclose(f);
    return
end

%% setup initial state

initcase = 'left';
switch initcase
  case 'middle'
    nc = G.cells.num;
    leftnc = floor(nc/2);
    rightnc = nc - leftnc;

    pressure_res = [pressure_left*ones(leftnc, 1); pressure_right*ones(rightnc, 1)];
    for ind = 1 : 3
        compmass_res{ind} = [compmass_left{ind}*ones(leftnc, 1); compmass_right{ind}*ones(rightnc, 1)];    
    end
    
  case 'left'
    nc = G.cells.num;

    pressure_res = pressure_right*ones(nc, 1);
    for ind = 1 : 3
        compmass_res{ind} = compmass_right{ind}*ones(nc, 1);    
    end

  case 'load'
    
    load('tempstates')
    laststate = states{end};
    pressure_res = model.getProp(laststate, {'core', 'pressure'});
    compmass_res = model.getProp(laststate, {'core', 'compmass'});
    
end

compstate = compmodel.initiateState([]);
compstate = compmodel.setProp(compstate, 'pressure', pressure_res);
compstate = compmodel.setProp(compstate, 'compmass', compmass_res);
[pcm_res, compstate] = compmodel.getUpdatedProp(compstate, 'phaseCompMass');
[s_res, compstate] = compmodel.getUpdatedProp(compstate, 's');

state = model.initiateState([]);
state = model.setProp(state, {'core', 'pressure'}, pressure_res);
state = model.setProp(state, {'core', 'phaseCompMass'}, pcm_res);
state = model.setProp(state, {'core', 's'}, s_res);

%% setup schedule

dt = 1e1;
dt = rampupTimesteps(10*dt, dt, 10);
schedule = simpleSchedule(dt, 'bc', bc);

nls = NonLinearSolver('errorOnFailure', false);

model.verbose = true;
states = simulateScheduleAD(state, model, schedule, 'NonLinearSolver', nls);

%%

states_new = {};
for i = 1:numel(states)
    if(not(isempty(states{i})))
        states_new{i} = states{i}; 
    end
end
states = states_new; 

figure
plotToolbar(model.G, states, 'plot1d', true);


