clear all
close all

mrstModule add compositional ad-core multimodel

T0 = 273.15;
T = 30; % in degrees
T = T + T0;

model = FlashCO2WaterModel(T);

p = [1; 600*(0.01 : 0.005 : 1)']*barsa;
n = size(p, 1);

mH2O = 1000;
mCO2 = 100;

compmass{1} = mH2O*ones(n, 1);
compmass{2} = mCO2*ones(n, 1); 

state.pressure = p;
state.compmass = compmass;

state = model.initiateState(state);

state = model.solveFlash(state);


phaseindex = model.getProp(state, 'phaseindex');
sA = model.getProp(state, 'sA');
p = model.getProp(state, 'pressure');

%% phase index
figure
plot(p/barsa, phaseindex)
title('phase index');

figure
plot(p/barsa, sA)
title('Aqueous saturation');
