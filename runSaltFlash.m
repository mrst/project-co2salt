clear all
close all

mrstModule add compositional ad-core 

T0 = 273.15;
T = 30; % in degrees
T = T + T0;

co2watermodel = CO2WaterModel(T);
adminmodel = AdminModel();
co2watermodel = co2watermodel.setupAdminModel(adminmodel);

model = CO2WaterSaltModel(T);

p = [1; 600*(0.01 : 0.005 : 1)']*barsa;
n = size(p, 1);

clear state
state.pressure = p;
state = co2watermodel.initiateState(state);

[rhoA, state] = co2watermodel.getUpdatedProp(state, 'rhoA');
[rhoG, state] = co2watermodel.getUpdatedProp(state, 'rhoG');

figure
plot(p/barsa, rhoA)
title('Aqueous density');

figure
plot(p/barsa, rhoG)
title('Co2 rich phase density');

mH2O = 1;
mCO2 = 1;

compmass{1} = mH2O*ones(n, 1);
compmass{2} = mCO2*ones(n, 1); 

molsalts = [0, 1, 2, 4]*mol;

ifig = 2;

for ind = 1 : numel(molsalts)
    
    compmass{3} = model.physconst.molarmass('NaCl').*molsalts(ind).*mH2O*ones(n, 1);

    clear state
    state.pressure = p;
    state.compmass = compmass;

    state = model.initiateState(state);
    state = model.computeFlash(state);
    
    sA = model.getProp(state, 'sA');
    mA = model.getProp(state, 'mA');
    p = model.getProp(state, 'pressure');
    pind = model.getProp(state, 'phaseindex');
    
    figure(ifig + 1)
    hold on
    plot(p/barsa, sA)
    title('Aqueous saturation');
    
    figure(ifig + 2)
    hold on
    plot(p/barsa, mA{3})
    title('Salt Mass Fraction');
    
    % figure(ifig + 3)
    % hold on
    % plot(p/barsa, pind)
    % title('Phase index Fraction');
    
end
